#!/usr/bin/env bash
set -euxo pipefail

# Create a temporary directory in which we'll work, and a cleanup function
TMP_DIR="$(mktemp -d)"
remove_tmp_folder() {
  rm -rf "${TMP_DIR}"
}
trap remove_tmp_folder EXIT
cd "${TMP_DIR}"

# This variable is set by the build-website job in our pipeline.
echo "Deploying artifacts from job ${DEPLOY_JOB_ID}"

# This variable is protected, so this script can succeed only on protected
# branches.
echo "$WWW_UPDATER_ID_RSA" | base64 -d > id_rsa
chmod 600 id_rsa

HOSTS=(api-north api-south api-proxy.grenoble api-proxy.lille api-proxy.louvain api-proxy.luxembourg api-proxy.lyon api-proxy.nancy api-proxy.nantes api-proxy.rennes api-proxy.sophia api-proxy.strasbourg api-proxy.toulouse)
FAILED=()

# Disable exit on error for this section: we want to try as many deployments as
# possible and reports which failed, we don't want to stop at the first failed.
set +e
for host in "${HOSTS[@]}"; do
  # NOTE: we may want to parametrize the branch here.
  # get_and_swap_www.sh is installed by puppet on the machine.
  # See https://gitlab.inria.fr/grid5000/grid5000-puppet/-/merge_requests/652.
  ssh -i id_rsa "www-updater@${host}" "get_and_swap_www.sh -p 52090 -t /var/www/resources-explorer -i ${DEPLOY_JOB_ID}" || FAILED+=("${host}")
done

if [ ${#FAILED[@]} -gt 0 ]; then
  echo "Deployment failed for the following hosts:" "${FAILED[@]}"
  exit 1
else
  echo "Deployments successful"
fi

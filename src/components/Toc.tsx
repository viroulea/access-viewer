import Accordion from '@mui/material/Accordion';
import AccordionDetails from '@mui/material/AccordionDetails';
import AccordionSummary from '@mui/material/AccordionSummary';
import Box from '@mui/material/Box';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Link from '@/components/InternalLink';
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import { ReactNode } from "react";
import Typography from '@mui/material/Typography';

type TocEntry = {
  element: ReactNode,
  anchor: string,
  subEntries?: TocEntry[],
};

function Entry({ entry, prefix }: { entry: TocEntry, prefix: string }) {
  const { element, anchor, subEntries } = entry;
  const anchorHref = `#${anchor}`;
  return (
    <>
      <ListItem disablePadding>
        {prefix}
        <Link href={anchorHref} sx={{ pl: 1 }}>{element}</Link>
      </ListItem>
      {subEntries && (
        <List component="div" disablePadding sx={{ pl: 3 }}>
          {subEntries.map((e, index) => {
            const subPrefix = `${prefix}.${index+1}`;
            return <Entry entry={e} prefix={subPrefix} key={index} />;
          })}
        </List>
      )}
    </>
  );
}

export default function Toc({ entries }: { entries: TocEntry[] }) {
  if (entries.length === 0) {
    return '';
  }
  return (
    <Box sx={{ display: 'flex', alignItems: 'flex-start', mb: 2 }}>
      <Accordion
        defaultExpanded
        variant="outlined"
        square
        disableGutters
        sx={{ backgroundColor: 'grey.100' }}
      >
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1-content"
          id="panel1-header"
        >
          <Typography variant="h6">Contents</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <List component="nav" disablePadding>
            {entries.map((e, index) =>
              <Entry entry={e} prefix={(index + 1).toString()} key={index} />
            )}
          </List>
        </AccordionDetails>
      </Accordion>
    </Box>
  );
}

import Autocomplete from '@mui/material/Autocomplete';
import CircularProgress from '@mui/material/CircularProgress';
import { GGA } from '@/lib/ggas';
import TextField from '@mui/material/TextField';

export type GroupType = GGA | null;

type Props = {
  group: GroupType,
  setGroup: React.Dispatch<GroupType>,
  allGroups: GGA[],
  loading: boolean,
};

export default function SelectGroup({
  group,
  setGroup,
  loading,
  allGroups,
}: Props) {
  return (
    <Autocomplete
      disablePortal
      value={group}
      onChange={(event, newValue) => setGroup(newValue)}
      options={allGroups}
      getOptionLabel={({ name }) => name}
      // This is necessary because the collection of "options" may get updated
      // during the life of the page, and the default equality operator compare
      // the objects!
      isOptionEqualToValue={(option, value) => option.name === value.name}
      renderInput={(params) =>
        <TextField {...params}
          label={loading ? "Loading..." : "Select group"}
          disabled={loading}
          InputProps={{
            ...params.InputProps,
            startAdornment: loading && (
              <CircularProgress color="inherit" size={20} />
            )
          }}
        />
      }
    />
  );
}

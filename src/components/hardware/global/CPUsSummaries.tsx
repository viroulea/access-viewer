import { ClusterNode, RefRepo, Site, getFamily } from "@grid5000/refrepo-ts";
import {
  TableConfiguration, TableDescription
} from "@/components/hardware/global/HardwareTableSummary";
import { Row } from "@/lib/table";
import TableGroup from "./TableGroup";


// Functions to group resources by arch

const HEADERS_ARCH = [
  { field: 'arch', element: 'Architecture' },
];

function getIdArch(node: ClusterNode) {
  return node.architecture.platform_type;
}

function createEmptyRowArch(node: ClusterNode): Row {
  return {
    id: { value: getIdArch(node) },
    arch: { value: node.architecture.platform_type },
  };
}

const CONFIG_ARCH: TableConfiguration = {
  getId: getIdArch,
  createEmptyRow: createEmptyRowArch,
  headers: HEADERS_ARCH,
};

// Functions to group resources by family

const HEADERS_FAMILY = [
  { field: 'vendor', element: 'Vendor' },
  { field: 'family', element: 'Family' },
  { field: 'arch', element: 'Architecture' },
];

function getIdFamily(node: ClusterNode) {
  const { processor, architecture } = node;
  return `${processor.vendor}-${getFamily(processor)}-${architecture.platform_type}`;
}

function createEmptyRowFamily(node: ClusterNode): Row {
  return {
    id: { value: getIdFamily(node) },
    arch: { value: node.architecture.platform_type },
    family: { value: getFamily(node.processor) },
    vendor: { value: node.processor.vendor },
  };
}

const CONFIG_FAMILY: TableConfiguration = {
  getId: getIdFamily,
  createEmptyRow: createEmptyRowFamily,
  headers: HEADERS_FAMILY,
};

// Functions to group resources by model

const HEADERS_MODEL = [
  { field: 'vendor', element: 'Vendor' },
  { field: 'model', element: 'Model' },
  { field: 'microarch', element: 'Microarchitecture' },
  { field: 'arch', element: 'Architecture' },
];

function getIdModel(node: ClusterNode) {
  const { processor, architecture } = node;
  const { vendor, model, microarchitecture, version } = processor;
  return `${vendor}-${model}-${microarchitecture}-${version}-${architecture.platform_type}`;
}

function createEmptyRowModel(node: ClusterNode): Row {
  const { processor, architecture } = node;
  const { vendor, model, microarchitecture, version } = processor;
  // FIXME: sort model according to the release date!
  return {
    id: { value: getIdModel(node) },
    arch: { value: architecture.platform_type },
    model: { value: `${getFamily(processor)}${version !== 'Unknown' ? version : ''}` },
    microarch: { value: microarchitecture },
    vendor: { value: vendor },
  };
}

const CONFIG_MODEL: TableConfiguration = {
  getId: getIdModel,
  createEmptyRow: createEmptyRowModel,
  headers: HEADERS_MODEL,
};


function accumulateRow(attr: 'nb_procs' | 'nb_cores',
  row: Row, node: ClusterNode, site: Site): Row {
  const val = node.architecture[attr];
  (row.total.value as number) += val;
  (row[site.uid].value as number) += val;
  return row;
}


const TABLES: TableDescription[] = [
  {
    title: 'CPU counts per architecture',
    anchor: 'cpus-cpu-arch',
    accumulator: accumulateRow.bind(null, 'nb_procs'),
    configuration: CONFIG_ARCH,
  },
  {
    title: 'Core counts per architecture',
    anchor: 'cpus-core-arch',
    accumulator: accumulateRow.bind(null, 'nb_cores'),
    configuration: CONFIG_ARCH,
  },
  {
    title: 'CPU counts per family',
    anchor: 'cpus-cpu-family',
    accumulator: accumulateRow.bind(null, 'nb_procs'),
    configuration: CONFIG_FAMILY,
  },
  {
    title: 'Core counts per family',
    anchor: 'cpus-core-family',
    accumulator: accumulateRow.bind(null, 'nb_cores'),
    configuration: CONFIG_FAMILY,
  },
  {
    title: 'CPU counts per model',
    anchor: 'cpus-cpu-model',
    accumulator: accumulateRow.bind(null, 'nb_procs'),
    configuration: CONFIG_MODEL,
  },
  {
    title: 'Core counts per model',
    anchor: 'cpus-core-model',
    accumulator: accumulateRow.bind(null, 'nb_cores'),
    configuration: CONFIG_MODEL,
  },
];

export default function CPUsSummaries({ refrepo }: { refrepo: RefRepo }) {
  return <TableGroup refrepo={refrepo} tables={TABLES} groupName="CPUs" groupAnchor="cpus"/>;
}

import { BasicHeader, Row, capitalize } from "@/lib/table";
import { Cluster, ClusterNode, RefRepo, Site } from "@grid5000/refrepo-ts";
import Box from "@mui/material/Box";
import { DataTable } from "@/components/hardware/common/DataTable";
import Link from "@/components/InternalLink";
import { useMemo } from "react";

type GetId = (node: ClusterNode, site: Site, cluster: Cluster) => string | string[];
export type AccumulateRow = (row: Row, node: ClusterNode, site: Site) => Row;
type CreateEmptyRow = (node: ClusterNode, id: string, site: Site, cluster: Cluster) => Row;

export type TableConfiguration = {
  getId: GetId,
  createEmptyRow: CreateEmptyRow,
  headers: BasicHeader[],
};

export type TableDescription = {
  title: string,
  anchor: string,
  accumulator: AccumulateRow,
  configuration: TableConfiguration,
  showSites?: boolean,
};

function computeTotalRow(headers: BasicHeader[], rows: Row[], sites: string[]): Row {
  const totalRow: Row = {};
  // Provide some safe initialization values.
  for (const header of headers) {
    totalRow[header.field] = { value: sites.includes(header.field) ? 0 : '' };
  }
  totalRow[headers[0].field] = { value: 'Sites total' };
  totalRow['total'] = { value: 0 };
  for (const row of rows) {
    for (const site of sites) {
      (totalRow[site].value as number) += (row[site].value as number);
    }
    (totalRow.total.value as number) += (row.total.value as number);
  }
  return totalRow;
}

type RowsById = { [id: string]: Row };

function computeRows(refrepo: RefRepo,
  showSites: boolean,
  accumulate: AccumulateRow,
  getId: GetId,
  createEmptyRow: CreateEmptyRow): Row[] {
  const rowsById: RowsById = {};
  const createRow = (node: ClusterNode, id: string, site: Site, cluster: Cluster) => {
    const basicRow = createEmptyRow(node, id, site, cluster);
    if (showSites) {
      basicRow.total = { value: 0 };
      Object.keys(refrepo.sites).forEach(s => basicRow[s] = { value: 0 });
    }
    return basicRow;
  };
  Object.values(refrepo.sites).forEach(site =>
    Object.values(site.clusters).forEach(cluster =>
      Object.values(cluster.nodes).forEach(node => {
        const nodeId = getId(node, site, cluster);
        // Sometime a single node may contribute to multiple rows.
        const ids = Array.isArray(nodeId) ? nodeId : [nodeId];
        for (const id of ids) {
          rowsById[id] ||= createRow(node, id, site, cluster);
          accumulate(rowsById[id], node, site);
        }
      })));
  const rows = Object.values(rowsById).filter(r => r.id.value !== '');
  return rows;
}

export default function HardwareTableSummary({
  refrepo,
  configuration,
  accumulate,
  showSites = true,
}: {
  refrepo: RefRepo,
  configuration: TableConfiguration,
  accumulate: AccumulateRow,
  showSites?: boolean,
}) {
  const { getId, headers, createEmptyRow } = configuration;
  const allSites = useMemo(() => showSites ? Object.keys(refrepo.sites) : [],
    [showSites, refrepo.sites]);
  const fullHeaders = useMemo(() => {
    const tmpHeaders = [
      ...headers,
      ...allSites.map(site => ({
        field: site,
        element: <Link href={`/hardware/${site}`}>{capitalize(site)}</Link>
      })),
    ];
    if (showSites) {
      tmpHeaders.push({ field: 'total', element: 'Total' });
    }
    return tmpHeaders;
  }, [headers, allSites, showSites]
  );
  const rows = useMemo(
    () => computeRows(refrepo, showSites, accumulate, getId, createEmptyRow),
    [refrepo, showSites, accumulate, getId, createEmptyRow]
  );
  const footer = useMemo(() =>
    showSites ? computeTotalRow(fullHeaders, rows, allSites) : undefined, [fullHeaders, rows, allSites, showSites]);
  return (
    <Box sx={{ mb: 2 }}>
      <DataTable
        rows={rows}
        columns={fullHeaders}
        tableFooter={footer}
      />
    </Box>
  );
}

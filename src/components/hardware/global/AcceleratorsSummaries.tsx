import { ClusterNode, RefRepo, Site } from "@grid5000/refrepo-ts";
import {
  TableConfiguration, TableDescription
} from "@/components/hardware/global/HardwareTableSummary";
import { Row } from "@/lib/table";
import TableGroup from "./TableGroup";
import { formatSize } from "@/lib/hardware";

// Functions to group resources by type

const HEADERS_TYPE = [
  { field: 'vendor', element: 'Vendor' },
  { field: 'type', element: 'Type' },
];

function getGPUModel(model: string) {
  let cleanedUp = model.replace('GeForce ', '');
  cleanedUp = cleanedUp.replace('Radeon Instinct ', '');
  if (cleanedUp.startsWith('A100-')) {
    return 'A100';
  }
  if (cleanedUp.startsWith('Tesla V100-')) {
    return 'Tesla V100';
  }
  if (cleanedUp.startsWith('Tesla P100-')) {
    return 'Tesla P100';
  }
  return cleanedUp;
}

function getInfo(node:ClusterNode) {
  if (node.gpu_devices) {
    const gpu = Object.values(node.gpu_devices)[0];
    return {
      vendor: gpu.vendor,
      type: 'GPU',
      model: getGPUModel(gpu.model),
      microarchitecture: gpu.microarchitecture,
      memory: gpu.memory,
      cc: gpu.compute_capability ? gpu.compute_capability : 'N/A',
    };
  } else if (node.other_devices) {
    const accel = Object.values(node.other_devices)[0];
    return {
      vendor: accel.vendor,
      type: accel.type,
      model: accel.model,
      microarchitecture: 'N/A',
      memory: accel.memory,
      cc: 'N/A',
    };
  }
  return {
    vendor: '',
    type: '',
    model: '',
    microarchitecture: '',
    memory: 0,
    cc: '',
  }
}

function getIdAccelerator(node: ClusterNode) {
  const { vendor, type } = getInfo(node);
  return vendor !== '' ?  `${vendor}-${type}` : '';
}

function createEmptyRowAccelerator(node: ClusterNode): Row {
  const id = getIdAccelerator(node);
  const { vendor, type } = getInfo(node);

  return {
    id: { value: id },
    vendor: { value: vendor },
    type: { value: type },
  };
}

const CONFIG_TYPE: TableConfiguration = {
  getId: getIdAccelerator,
  createEmptyRow: createEmptyRowAccelerator,
  headers: HEADERS_TYPE,
};

function getNumberOfAccelerators(node: ClusterNode) {
  if (node.gpu_devices) {
    return Object.keys(node.gpu_devices).length;
  } else if (node.other_devices) {
    return Object.keys(node.other_devices).length;
  }
  return 0;
}

function accumulateRow(row: Row, node: ClusterNode, site: Site): Row {
  const nAccels = getNumberOfAccelerators(node);
  (row.total.value as number) += nAccels;
  (row[site.uid].value as number) += nAccels;
  return row;
}

// Functions to group resources by model

const HEADERS_MODEL = [
  { field: 'vendor', element: 'Vendor' },
  { field: 'type', element: 'Type' },
  { field: 'model', element: 'Model' },
  { field: 'microarchitecture', element: 'Microarchitecture' },
  { field: 'memory', element: 'Memory' },
  { field: 'cc', element: 'Compute capability' },
];

function getIdModel(node: ClusterNode) {
  const { vendor, type, model } = getInfo(node);
  return vendor !== '' ?  `${vendor}-${type}-${model}` : '';
}

function createEmptyRowModel(node: ClusterNode): Row {
  const id = getIdModel(node);
  const { vendor, type, model, microarchitecture, memory, cc } = getInfo(node);

  return {
    id: { value: id },
    vendor: { value: vendor },
    type: { value: type },
    model: { value: model },
    microarchitecture: { value: microarchitecture },
    memory: { value: memory, element: formatSize(memory, 'IEC', 0) },
    cc: { value: cc },
  };
}

const CONFIG_MODEL: TableConfiguration = {
  getId: getIdModel,
  createEmptyRow: createEmptyRowModel,
  headers: HEADERS_MODEL,
};

// Functions to group resources by GPU model

const HEADERS_GPU_MODEL = [
  { field: 'vendor', element: 'Vendor' },
  { field: 'model', element: 'Model' },
  { field: 'microarchitecture', element: 'Microarchitecture' },
  { field: 'memory', element: 'Memory' },
  { field: 'cc', element: 'Compute capability' },
];

function getIdGPUModel(node: ClusterNode) {
  const { vendor, type, model } = getInfo(node);
  if (type !== 'GPU') {
    return '';
  }
  return `${vendor}-${type}-${model}`;
}

const CONFIG_GPU_MODEL: TableConfiguration = {
  getId: getIdGPUModel,
  createEmptyRow: createEmptyRowModel,
  headers: HEADERS_GPU_MODEL,
};

function accumulateRowCores(row: Row, node: ClusterNode, site: Site): Row {
  if (!node.gpu_devices) {
    return row;
  }
  const cores = Object.values(node.gpu_devices)
    .reduce((sum, { cores }) => sum + cores, 0);
  (row.total.value as number) += cores;
  (row[site.uid].value as number) += cores;
  return row;
}

const TABLES: TableDescription[] = [
  {
    title: 'Accelerators counts per type',
    anchor: 'accel-type',
    accumulator: accumulateRow,
    configuration: CONFIG_TYPE,
  },
  {
    title: 'Accelerators counts per model',
    anchor: 'accel-model',
    accumulator: accumulateRow,
    configuration: CONFIG_MODEL,
  },
  {
    title: 'GPU core counts per model',
    anchor: 'accel-gpu-cores',
    accumulator: accumulateRowCores,
    configuration: CONFIG_GPU_MODEL,
  },
];

export default function AcceleratorsSummaries({ refrepo }: { refrepo: RefRepo }) {
  return (
    <div id="accel">
      <TableGroup
        refrepo={refrepo}
        tables={TABLES}
        groupName="Accelerators (GPU, Xeon Phi, FPGA)"
      />
    </div>
  );
}

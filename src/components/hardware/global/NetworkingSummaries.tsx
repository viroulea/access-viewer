import { Cluster, ClusterNode, Network, RefRepo, Site } from "@grid5000/refrepo-ts";
import {
  TableConfiguration, TableDescription
} from "@/components/hardware/global/HardwareTableSummary";
import { formatRate, visibleNetwork } from "@/lib/hardware";
import Box from "@mui/material/Box";
import { Row } from "@/lib/table";
import TableGroup from "./TableGroup";

// Functions to group resources by interconnect type

const HEADERS_INTERCO = [
  { field: 'network', element: 'Interconnect' },
];

function formatNetwork(network: Network) {
  const { rate } = network;
  return `${network.interface} ${formatRate(rate || 0)}`;
}

function getIdInterconnect(node: ClusterNode) {
  const networks = node.network_adapters.filter(visibleNetwork);
  return networks.map(formatNetwork);
}

function createEmptyRowInterco(node: ClusterNode, id: string): Row {
  return {
    id: { value: id },
    // FIXME: get and sort by size
    network: { value: id },
  };
}

const CONFIG_INTERCO: TableConfiguration = {
  getId: getIdInterconnect,
  createEmptyRow: createEmptyRowInterco,
  headers: HEADERS_INTERCO,
};

function accumulateRow(row: Row, node: ClusterNode, site: Site): Row {
  (row.total.value as number) += 1;
  (row[site.uid].value as number) += 1;
  return row;
}

// Functions to group resources by site + clusters interfaces
const HEADERS_MULTIPLE = [
  { field: 'site', element: 'Site' },
  { field: 'cluster', element: 'Cluster' },
  { field: 'nodes', element: 'Nodes' },
  { field: '25g', element: '25G interfaces' },
  { field: '10g', element: '10G interfaces' },
  { field: '1g', element: '1G interfaces' },
  { field: 'interfaces', element: 'Interfaces (throughput)' },
];

function visibleEthernet(network: Network) {
  return visibleNetwork(network) && network.interface === 'Ethernet';
}

function formatNetworkInterface(network: Network) {
  return `${network.name} (${formatRate(network.rate || 0)})`;
}

function createEmptyRowInterfaces(node: ClusterNode, id: string, site: Site, cluster: Cluster): Row {
  const networks = node.network_adapters
    .filter(visibleEthernet)

  const ifaces = networks
    .map(formatNetworkInterface)
    .join(', ');

  let g1 = 0;
  let g10 = 0;
  let g25 = 0;
  for (const net of networks) {
    const rateInGbps = (net.rate || 0) / 10 ** 9;
    switch (rateInGbps) {
    case 1:
      g1 += 1;
      break;
    case 10:
      g10 += 1;
      break;
    case 25:
      g25 += 1;
      break;
    }
  }

  return {
    id: { value: id },
    site: { value: site.uid },
    cluster: { value: cluster.uid },
    nodes: { value: 0 },
    '25g': { value: g25 },
    '10g': { value: g10 },
    '1g': { value: g1 },
    interfaces: { value: ifaces },
  };
}

function getIdInterfaces(node: ClusterNode, site: Site, cluster: Cluster) {
  const ethNetworks = node.network_adapters.filter(visibleEthernet);
  if (ethNetworks.length < 2) {
    return '';
  }
  return `${site.uid}-${cluster.uid}`;
}

function accumulateRowNodes(row: Row, node: ClusterNode): Row {
  (row.nodes.value as number) += 1;
  return row;
}

const CONFIG_INTERFACES: TableConfiguration = {
  getId: getIdInterfaces,
  createEmptyRow: createEmptyRowInterfaces,
  headers: HEADERS_MULTIPLE,
};

// Functions to group resources by site + clusters SR-IOV
const HEADERS_SRIOV = [
  { field: 'site', element: 'Site' },
  { field: 'cluster', element: 'Cluster' },
  { field: 'nodes', element: 'Nodes' },
  { field: 'interfaces', element: 'Interfaces (max number of Virtual Functions)' },
];

function visibleSRIOV(network: Network) {
  return visibleNetwork(network) && network.sriov;
}

function getIdSRIOV(node: ClusterNode, site: Site, cluster: Cluster) {
  const networks = node.network_adapters.filter(visibleSRIOV);
  if (networks.length === 0) {
    return '';
  }
  return `${site.uid}-${cluster.uid}`;
}

function formatNetworkSRIOV(network: Network) {
  return `${network.name} (${network.sriov_totalvfs})`;
}

function createEmptyRowSRIOV(node: ClusterNode, id: string, site: Site, cluster: Cluster): Row {
  const networks = node.network_adapters
    .filter(visibleSRIOV)

  const ifaces = networks
    .map(formatNetworkSRIOV)
    .join(', ');

  return {
    id: { value: id },
    site: { value: site.uid },
    cluster: { value: cluster.uid },
    nodes: { value: 0 },
    interfaces: { value: ifaces },
  };
}

const CONFIG_SRIOV: TableConfiguration = {
  getId: getIdSRIOV,
  createEmptyRow: createEmptyRowSRIOV,
  headers: HEADERS_SRIOV,
};

// Functions to group resources by site + clusters SR-IOV
const HEADERS_MODEL = [
  { field: 'type', element: 'Type' },
  { field: 'driver', element: 'Driver' },
  { field: 'model', element: 'Model' },
];

function formatModel(net: Network) {
  const model = net.model || 'N/A';
  const vendor = net.vendor || 'N/A';
  return `${vendor} ${model}`;
}

function getIdModel(node: ClusterNode) {
  const networks = node.network_adapters.filter(visibleNetwork);
  if (networks.length < 1) {
    return '';
  }
  const networkById: { [id: string]: number } = {};
  for (const net of networks) {
    networkById[formatModel(net)] = 1;
  }
  return Object.keys(networkById);
}

function createEmptyRowModel(node: ClusterNode, id: string): Row {
  const net = node.network_adapters.filter(visibleNetwork).find(n => formatModel(n) === id);
  const iface = net ? net.interface : '';
  return {
    id: { value: id },
    type: { value: iface.startsWith('FPGA') ? `${iface}*` : iface },
    driver: { value: net ? net.driver : '' },
    model: { value: id },
  };
}

const CONFIG_MODEL: TableConfiguration = {
  getId: getIdModel,
  createEmptyRow: createEmptyRowModel,
  headers: HEADERS_MODEL,
};

const TABLES: TableDescription[] = [
  {
    title: 'Network interconnects',
    anchor: 'network-interconnects',
    accumulator: accumulateRow,
    configuration: CONFIG_INTERCO,
  },
  {
    title: 'Nodes with several Ethernet interfaces',
    anchor: 'network-multiple-interfaces',
    accumulator: accumulateRowNodes,
    configuration: CONFIG_INTERFACES,
    showSites: false,
  },
  {
    title: 'Nodes with SR-IOV support',
    anchor: 'network-sriov',
    accumulator: accumulateRowNodes,
    configuration: CONFIG_SRIOV,
    showSites: false,
  },
  {
    title: 'Network interface models',
    anchor: 'network-models',
    accumulator: accumulateRow,
    configuration: CONFIG_MODEL,
  },
];

export default function NetworkingSummaries({ refrepo }: { refrepo: RefRepo }) {
  return (
    <>
      <TableGroup refrepo={refrepo} tables={TABLES} groupName="Networking" groupAnchor="networking" />
      <Box sx={{mb: 4}}>
        <i>
          *: Ethernet interfaces on FPGA cards are not directly usable by the operating system
        </i>
      </Box>
    </>
  );
}

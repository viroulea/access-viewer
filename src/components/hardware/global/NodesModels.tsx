import { Cluster, ClusterNode, RefRepo, Site } from "@grid5000/refrepo-ts";
import {
  TableConfiguration, TableDescription
} from "@/components/hardware/global/HardwareTableSummary";
import { Row } from "@/lib/table";
import TableGroup from "./TableGroup";

// Functions to group resources by nodes models

const HEADERS_NODES = [
  { field: 'model', element: 'Nodes model' },
];

function getId(node: ClusterNode, site: Site, cluster: Cluster) {
  return cluster.model;
}

function createEmptyRow(node: ClusterNode, id: string, site: Site, cluster: Cluster): Row {
  const { model } = cluster;
  return {
    id: { value: id },
    model: { value: model },
  };
}

const CONFIG_NODES: TableConfiguration = {
  getId: getId,
  createEmptyRow: createEmptyRow,
  headers: HEADERS_NODES,
};

function accumulateRow(row: Row, node: ClusterNode, site: Site): Row {
  (row.total.value as number) += 1;
  (row[site.uid].value as number) += 1;
  return row;
}

const TABLES: TableDescription[] = [
  {
    title: 'Nodes models',
    anchor: 'nodes-models',
    accumulator: accumulateRow,
    configuration: CONFIG_NODES,
  },
];

export default function NodesModels({ refrepo }: { refrepo: RefRepo }) {
  return <TableGroup refrepo={refrepo} tables={TABLES} />;
}

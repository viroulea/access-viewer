import HardwareTableSummary, { TableDescription
} from "@/components/hardware/global/HardwareTableSummary";
import Box from "@mui/material/Box";
import Divider from "@mui/material/Divider";
import Link from '@/components/InternalLink';
import { RefRepo } from "@grid5000/refrepo-ts";
import Typography from "@mui/material/Typography";
import styles from "@/components/Links.module.css";

export default function TableGroup({ refrepo, tables, groupName, groupAnchor }: {
  refrepo: RefRepo,
  groupName?: string,
  groupAnchor?: string,
  tables: TableDescription[],
}) {
  return (
    <>
      {groupName && groupAnchor && (
        <Typography variant="h3" id={groupAnchor}>
          <Link className={styles.headerLink} href={`#${groupAnchor}`}>{groupName}</Link>
        </Typography>
      )}
      <Divider sx={{mb: 2}} />
      {tables.map(({ anchor, title, accumulator, configuration, showSites }) => (
        <Box key={title} sx={{mb: 4}}>
          <Typography variant="h4" id={anchor}>
            <Link className={styles.headerLink} href={`#${anchor}`}>{title}</Link>
          </Typography>
          <Divider sx={{mb: 2}} />
          <HardwareTableSummary
            refrepo={refrepo}
            accumulate={accumulator}
            configuration={configuration}
            showSites={showSites}
          />
        </Box>
      ))}
    </>
  );
}

import { ClusterNode, RefRepo, Site } from "@grid5000/refrepo-ts";
import {
  TableConfiguration, TableDescription
} from "@/components/hardware/global/HardwareTableSummary";
import { formatSize, isDram, isPmem, sizeReducer } from "@/lib/hardware";
import { Row } from "@/lib/table";
import TableGroup from "./TableGroup";

// Functions to group resources by RAM

const HEADERS_RAM = [
  { field: 'mem', element: 'RAM size' },
];

function getMem(ramType: 'dram' | 'pmem', node: ClusterNode) {
  const mem = ramType === 'dram' ? isDram : isPmem;
  return node.memory_devices.filter(mem).reduce(sizeReducer, 0);
}

function getIdMem(ramType: 'dram' | 'pmem', node: ClusterNode) {
  const mem = getMem(ramType, node);
  return `${mem > 0 ? mem : ''}`;
}

function createEmptyRowMem(ramType: 'dram' | 'pmem', node: ClusterNode): Row {
  const id = getIdMem(ramType, node);
  const mem = getMem(ramType, node);

  return {
    id: { value: id },
    mem: { value: mem, element: formatSize(mem, 'IEC', 1) },
  };
}

const CONFIG_RAM: TableConfiguration = {
  getId: getIdMem.bind(null, 'dram'),
  createEmptyRow: createEmptyRowMem.bind(null, 'dram'),
  headers: HEADERS_RAM,
};

const HEADERS_PMEM = [
  { field: 'mem', element: 'PMEM size' },
];

const CONFIG_PMEM: TableConfiguration = {
  getId: getIdMem.bind(null, 'pmem'),
  createEmptyRow: createEmptyRowMem.bind(null, 'pmem'),
  headers: HEADERS_PMEM,
};


function accumulateRow(row: Row, node: ClusterNode, site: Site): Row {
  (row.total.value as number) += 1;
  (row[site.uid].value as number) += 1;
  return row;
}


const TABLES: TableDescription[] = [
  {
    title: 'RAM size per node',
    anchor: 'mem-ram',
    accumulator: accumulateRow,
    configuration: CONFIG_RAM,
  },
  {
    title: 'PMEM size per node',
    anchor: 'mem-pmem',
    accumulator: accumulateRow,
    configuration: CONFIG_PMEM,
  },
];

export default function MemorySummaries({ refrepo }: { refrepo: RefRepo }) {
  return <TableGroup refrepo={refrepo} tables={TABLES} groupName="Memory" groupAnchor="memory" />;
}

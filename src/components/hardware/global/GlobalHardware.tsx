import { Cluster, RefRepo } from '@grid5000/refrepo-ts';
import { computeSummary, inDefault, inProduction, inTesting } from '@/lib/hardware';

import AcceleratorsSummaries from '@/components/hardware/global/AcceleratorsSummaries';
import Box from '@mui/material/Box';
import CPUsSummaries from '@/components/hardware/global/CPUsSummaries';
import ClustersTable from '@/components/hardware/common/ClustersTable';
import Container from '@mui/material/Container';
import Divider from '@mui/material/Divider';
import Grid from '@mui/material/Unstable_Grid2';
import Link from '@/components/InternalLink';
import MemorySummaries from '@/components/hardware/global/MemorySummaries';
import Menu from '@/components/hardware/common/Menu';
import NetworkingSummaries from '@/components/hardware/global/NetworkingSummaries';
import NodesModels from '@/components/hardware/global/NodesModels';
import ShowSummary from '@/components/hardware/common/ShowSummary';
import StorageSummaries from '@/components/hardware/global/StorageSummaries';
import Toc from '@/components/Toc';
import Typography from '@mui/material/Typography';
import { getSitesUids } from '@/lib/refrepo';
import styles from "@/components/Links.module.css";

function Clusters({ clusters }: { clusters: Cluster[] }) {
  return <div>{clusters.map(({ uid }) => uid).join(', ')}</div>;
}

const TOC_GLOBAL = [
  {
    element: 'Summary',
    anchor: 'summary',
    subEntries: [
      { element: 'Default queue resources', anchor: 'summary-default' },
      { element: 'Production queue resources', anchor: 'summary-production' },
    ],
  },
  {
    element: 'Clusters',
    anchor: 'clusters',
    subEntries: [
      { element: 'Default queue resources', anchor: 'clusters-default' },
      { element: 'Production queue resources', anchor: 'clusters-production' },
      { element: 'Testing queue resources', anchor: 'clusters-testing' },
    ],
  },
  {
    element: 'CPUs',
    anchor: 'cpus',
    subEntries: [
      { element: 'CPU counts per architecture', anchor: 'cpus-cpu-arch' },
      { element: 'Core counts per architecture', anchor: 'cpus-core-arch' },
      { element: 'CPU counts per family', anchor: 'cpus-cpu-family' },
      { element: 'Core counts per family', anchor: 'cpus-core-family' },
      { element: 'CPU counts per model', anchor: 'cpus-cpu-model' },
      { element: 'Core counts per model', anchor: 'cpus-core-model' },
    ],
  },
  {
    element: 'Memory',
    anchor: 'memory',
    subEntries: [
      { element: 'RAM size per node', anchor: 'mem-ram' },
      { element: 'PMEM size per node', anchor: 'mem-pmem' },
    ],
  },
  {
    element: 'Accelerators (GPU, Xeon Phi, FPGA)',
    anchor: 'accel',
    subEntries: [
      { element: 'Accelerator counts per type', anchor: 'accel-type' },
      { element: 'Accelerator counts per model', anchor: 'accel-model' },
      { element: 'GPU core counts per GPU model', anchor: 'accel-gpu-cores' },
    ],
  },
  {
    element: 'Networking',
    anchor: 'networking',
    subEntries: [
      { element: 'Network interconnects', anchor: 'network-interconnects' },
      { element: 'Nodes with several Ethernet interfaces', anchor: 'network-multiple-interfaces' },
      { element: 'Nodes with SR-IOV support', anchor: 'network-sriov' },
      { element: 'Network interface models', anchor: 'network-models' },
    ],
  },
  {
    element: 'Storage',
    anchor: 'storage',
    subEntries: [
      { element: 'SSD models', anchor: 'storage-ssd' },
      { element: 'Nodes with several disks', anchor: 'storage-multiple-disks' },
    ],
  },
  {
    element: 'Nodes models',
    anchor: 'nodes-models',
  },
];

function ResourcesHeader({ title, anchor }: { title: string, anchor: string}) {
  const anchorHref = `#${anchor}`;
  return (
    <Typography
      variant="h4"
      gutterBottom
      id={anchor}
    >
      <Link
        className={styles.headerLink}
        href={anchorHref}
      >
        {title}
      </Link>
    </Typography>
  )
}

export default function GlobalHardware({ refrepo }: { refrepo: RefRepo }) {
  const sites = Object.values(refrepo.sites);
  const summary = computeSummary(refrepo);
  const prodSummary = computeSummary(refrepo, inProduction);
  const defaultSummary = computeSummary(refrepo, inDefault);
  return (
    <>
      <Menu
        sites={getSitesUids(refrepo)}
        active="global"
      />
      <Container maxWidth={false}>
        <Typography variant="h3" gutterBottom>Global Hardware</Typography>
        <Toc entries={TOC_GLOBAL} />
        <Typography variant="h4" gutterBottom id="summary">Summary</Typography>
        <Divider sx={{mb: 2}}/>
        <Grid container spacing={2} sx={{mb: 2}}>
          <Grid xs={12} sm={6} md={4}>
            <ShowSummary summary={summary} title="Whole infrastructure" />
          </Grid>
          <Grid xs={12} sm={6} md={4} id="summary-production">
            <ShowSummary summary={prodSummary} title="Abaca (Production queue)" />
          </Grid>
          <Grid xs={12} sm={6} md={4} id="summary-default">
            <ShowSummary summary={defaultSummary} title="SLICES-FR (Default queue)" />
          </Grid>
        </Grid>
        <div id="clusters" />
        <ResourcesHeader title="Resources in default queue" anchor="clusters-default" />
        <Box sx={{mb:2}}>
          <ClustersTable refrepo={refrepo} filterCluster={inDefault} />
        </Box>
        <Typography
          variant="h4"
          gutterBottom
          id="clusters-production"
        >
          Resources in production queue
        </Typography>
        <Box sx={{mb:2}}>
          <ClustersTable
            refrepo={refrepo}
            filterCluster={inProduction}
            disableAccessCondition
          />
        </Box>
        <Typography
          variant="h4"
          gutterBottom
          id="clusters-testing"
        >
          Resources in testing queue
        </Typography>
        <ClustersTable
          refrepo={refrepo}
          filterCluster={inTesting}
          disableAccessCondition
        />
        <CPUsSummaries refrepo={refrepo} />
        <MemorySummaries refrepo={refrepo} />
        <AcceleratorsSummaries refrepo={refrepo} />
        <NetworkingSummaries refrepo={refrepo} />
        <StorageSummaries refrepo={refrepo} />
        <NodesModels refrepo={refrepo} />
      </Container>
    </>
  );
}

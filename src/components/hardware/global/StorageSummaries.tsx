import { Cluster, ClusterNode, RefRepo, Site, Storage } from "@grid5000/refrepo-ts";
import {
  TableConfiguration, TableDescription
} from "@/components/hardware/global/HardwareTableSummary";
import { formatSize, isHdd, isPrimary, isSsd } from "@/lib/hardware";
import { Row } from "@/lib/table";
import TableGroup from "./TableGroup";

function formatSizeOnly(disk?: Storage) {
  if (!disk) return '';
  const { size } = disk;
  const fixed = size < 10**12 ? 0 : 2;
  return formatSize(size, 'metric', fixed);
}

// Functions to group resources by SSD models

const HEADERS_SSD = [
  { field: 'interface', element: 'SSD interface' },
  { field: 'model', element: 'Model' },
  { field: 'size', element: 'Size' },
];

function createDiskId(disk: Storage) {
  const { size, vendor, model } = disk;
  return `${vendor}-${model}-${size}`;
}

function getIdsDisks(node: ClusterNode) {
  const disks = node.storage_devices.filter(isSsd);
  if (disks.length === 0) {
    return '';
  }
  return disks.map(createDiskId);
}

function createEmptyRowDisk(node: ClusterNode, id: string): Row {
  const disk = node.storage_devices.filter(isSsd).find(disk => createDiskId(disk) === id);
  const iface = disk ? disk.interface : '';
  const model = disk ? `${disk.vendor} ${disk.model}` : '';
  const size = disk ? disk.size : 0;
  return {
    id: { value: id },
    interface: { value: iface },
    model: { value: model },
    size: { value: size, element: formatSizeOnly(disk) },
  };
}

const CONFIG_SSD: TableConfiguration = {
  getId: getIdsDisks,
  createEmptyRow: createEmptyRowDisk,
  headers: HEADERS_SSD,
};

function accumulateRow(row: Row, node: ClusterNode, site: Site): Row {
  (row.total.value as number) += 1;
  (row[site.uid].value as number) += 1;
  return row;
}

// Functions to group resources by nodes (when they have multiple storages)

const HEADERS_MULTIPLE = [
  { field: 'site', element: 'Site' },
  { field: 'cluster', element: 'Cluster' },
  { field: 'nodes', element: 'Number of nodes' },
  { field: 'main', element: 'Main disk' },
  { field: 'moreHDD', element: 'Additional HDDs' },
  { field: 'moreSSD', element: 'Additional SSDs' },
];

function getIdCluster(node: ClusterNode, site: Site, cluster: Cluster) {
  if (node.storage_devices.length < 2) {
    return '';
  }
  return `${site.uid}-${cluster.uid}`;
}

function formatMain(disk?: Storage) {
  if (!disk) {
    return '';
  }
  return `${disk.interface} ${formatSizeOnly(disk)}`;
}

function formatExtras(disks: Storage[]) {
  if (disks.length === 0) {
    return '0';
  }
  return `${disks.length} (${disks.map(formatSizeOnly).join(', ')})`;
}

function createEmptyRowMultiple(node: ClusterNode, id: string, site: Site, cluster: Cluster): Row {
  const mainDisk = node.storage_devices.find(isPrimary);
  const extraHDD = node.storage_devices.filter(s => isHdd(s) && !isPrimary(s));
  const extraSSD = node.storage_devices.filter(s => isSsd(s) && !isPrimary(s));
  return {
    id: { value: id },
    site: { value: site.uid },
    cluster: { value: cluster.uid },
    nodes: { value: Object.keys(cluster.nodes).length },
    main: { value: formatMain(mainDisk) },
    moreHDD: { value: formatExtras(extraHDD) },
    moreSSD: { value: formatExtras(extraSSD) },
  };
}

const CONFIG_MULTIPLE: TableConfiguration = {
  getId: getIdCluster,
  createEmptyRow: createEmptyRowMultiple,
  headers: HEADERS_MULTIPLE,
};


const TABLES: TableDescription[] = [
  {
    title: 'SSD models',
    anchor: 'storage-ssd',
    accumulator: accumulateRow,
    configuration: CONFIG_SSD,
  },
  {
    title: 'Nodes with several disks',
    anchor: 'storage-multiple-disks',
    accumulator: (row: Row) => row,
    configuration: CONFIG_MULTIPLE,
    showSites: false,
  },
];

export default function StorageSummaries({ refrepo }: { refrepo: RefRepo }) {
  return <TableGroup refrepo={refrepo} tables={TABLES} groupName="Storage" groupAnchor="storage" />;
}

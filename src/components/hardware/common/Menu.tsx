import Button from "@mui/material/Button";
import Grid from "@mui/material/Unstable_Grid2";
import Link from "next/link";

function pathForPage(page: string) {
  switch (page) {
  case "global":
    return "/hardware";
  default:
    return `/hardware/${page}`;
  }
}

type Props = {
  sites: string[],
  active: string,
};

export function Loading() {
  return <div>Loading...</div>;
}

export default function Menu({
  sites, active
}: Props) {
  const menuElements = ['global', ...sites];
  return (
    <Grid sx={{ m: 2 }} container spacing={2} columns={4 * menuElements.length}>
      {menuElements.map(value => (
        <Grid
          xs={2 * menuElements.length}
          sm={menuElements.length}
          md={menuElements.length / 2}
          lg
          key={value}
          display="flex"
          justifyContent="center"
        >
          <Button
            component={Link}
            href={pathForPage(value)}
            fullWidth
            variant={value === active ? "contained" : "outlined"}
            color="primary"
          >
            {value}
          </Button>
        </Grid>
      ))}
    </Grid>
  );
}

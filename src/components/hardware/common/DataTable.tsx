"use client";

import { BasicHeader, GroupedHeaders, Header, Order, Row, getComparator, isGroupedHeader } from '@/lib/table';
import { MouseEvent, useMemo, useState } from 'react';

import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableFooter from '@mui/material/TableFooter';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import TableSortLabel from '@mui/material/TableSortLabel';
import styles from './DataTable.module.css';

interface EnhancedTableProps {
  onRequestSort: (event: MouseEvent<unknown>, property: string) => void;
  order: Order;
  orderBy: string;
  headers: readonly Header[];
}

function EnhancedTableHead({
  order, orderBy, onRequestSort, headers
}: EnhancedTableProps) {
  const createSortHandler =
    (property: string) => (event: MouseEvent<unknown>) => {
      onRequestSort(event, property);
    };

  const hasGroupedHeaders = headers.some(isGroupedHeader);
  return (
    <TableHead>
      <TableRow>
        {headers.map((header, index) => {
          const isGrouped = isGroupedHeader(header);
          const colSpan = isGrouped ? (header as GroupedHeaders).headers.length : 1;
          const rowSpan = hasGroupedHeaders && !isGrouped ? 2 : 1;
          const field = (header as BasicHeader).field;
          return (
            <TableCell
              key={index}
              sx={{ pr: 0 }}
              colSpan={colSpan}
              rowSpan={rowSpan}
              sortDirection={!isGrouped && orderBy === field ? order : false}
            >
              {isGrouped ?
                header.element
                : (
                  <TableSortLabel
                    active={orderBy === field}
                    direction={orderBy === field ? order : 'asc'}
                    onClick={createSortHandler(field)}
                    className={styles.sortLabel}
                  >
                    {header.element}
                  </TableSortLabel>
                )}
            </TableCell>
          )})}
      </TableRow>
      {hasGroupedHeaders && (
        <TableRow>
          {headers.map((header, index) => {
            const isGrouped = isGroupedHeader(header);
            if (!isGrouped) return '';
            const group = header as GroupedHeaders;
            return group.headers.map(({ element, field }, gIndex) => (
              <TableCell
                sx={{ pr: 0 }}
                key={`${index}-${gIndex}`}
                className={styles.nestedCell}
              >
                <TableSortLabel
                  active={orderBy === field}
                  direction={orderBy === field ? order : 'asc'}
                  onClick={createSortHandler(field)}
                  className={styles.sortLabel}
                >
                  {element}
                </TableSortLabel>
              </TableCell>
            ));
          })}
        </TableRow>
      )}
    </TableHead>
  );
}

export function DataTable({
  rows, columns,
  size = 'small',
  initialOrder = 'asc',
  tableFooter = undefined,
}: {
  rows: Row[],
  columns: readonly Header[],
  initialOrder?: Order,
  size?: 'medium' | 'small',
  tableFooter?: Row,
}) {
  const flattenedColumns: BasicHeader[] = useMemo(() => columns.flatMap(h => {
    if (isGroupedHeader(h)) {
      return (h as GroupedHeaders).headers;
    } else {
      return h as BasicHeader;
    }
  }), [columns]);
  const [order, setOrder] = useState<Order>(initialOrder);
  const [orderBy, setOrderBy] = useState<string>(
    flattenedColumns.length > 0 ? flattenedColumns[0].field : '');

  const handleRequestSort = (event: MouseEvent<unknown>, property: string) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const visibleRows = useMemo(() => rows.sort(getComparator(order, orderBy)),
    [order, orderBy, rows]);

  const getElement = (r: Row, f: string) =>
    r[f].element === undefined ? r[f].value : r[f].element;

  // FIXME: custom renderer for field (allow better sorting, and more flexibility).
  return (
    <TableContainer component={Paper}>
      <Table size={size} className={styles.mainTable}>
        <EnhancedTableHead
          order={order}
          orderBy={orderBy}
          onRequestSort={handleRequestSort}
          headers={columns}
        />
        <TableBody>
          {visibleRows.map((row, index) => (
            <TableRow hover key={`row-${row.id.value as string}`}>
              {flattenedColumns.map(({ field }, index) =>(
                <TableCell key={index} className={field === 'total' ? styles.totalCell : ''}>
                  {getElement(row, field)}
                </TableCell>
              ))}
            </TableRow>
          ))}
        </TableBody>
        {tableFooter && (
          <TableFooter className={styles.footer}>
            <TableRow>
              {flattenedColumns.map(({ field }, index) =>(
                <TableCell key={index}>{getElement(tableFooter, field)}</TableCell>
              ))}
            </TableRow>
          </TableFooter>
        )}
      </Table>
    </TableContainer>
  );
}

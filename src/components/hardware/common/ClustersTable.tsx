import { BasicHeader, Header, Row, capitalize, getComparator } from '@/lib/table';
import { Cluster, ClusterNode, GPUDevice, Network, RefRepo, Storage, normalizeClusterName } from '@grid5000/refrepo-ts';

import { ReactNode, useMemo } from 'react';
import { computeIntervals, createGPUIdentifier, formatDate, formatIntervals, formatMem, formatOther, formatRate, formatSize, groupGPUsById, isDram, isPmem, isSupported, nodeIdAsNumber, sizeReducer, visibleNetwork } from '@/lib/hardware';
import { DataTable } from '@/components/hardware/common/DataTable';
import ExternalLink from '@/components/ExternalLink';
import GPU from './GPU';
import Link from '@/components/InternalLink';

const RESERVATION_URL = "https://www.grid5000.fr/w/Disk_reservation";

const HEADERS: Header[] = [
  { field: 'site', element: 'Site' },
  { field: 'cluster', element: 'Cluster' },
  { field: 'access', element: 'Access Condition' },
  { field: 'creation', element: 'Date of arrival' },
  { field: 'manufactured', element: 'Manufacturing date' },
  { field: 'nodes', element: 'Nodes' },
  {
    element: 'CPU',
    headers: [
      { field: 'nCPUs', element: '#' },
      { field: 'CPUName', element: 'Name' },
      { field: 'nCPUCores', element: 'Cores' },
      { field: 'arch', element: 'Architecture' },
    ]
  },
  { field: 'memory', element: 'Memory' },
  { field: 'storage', element: 'Storage' },
  { field: 'network', element: 'Network' },
  { field: 'accelerators', element: 'Accelerators' },
];

function formatNetwork(network: Network) {
  const { rate, sriov } = network;
  const iSuffix = network.interface === 'Ethernet' ? '' : ` ${network.interface}`;
  const sriovSuffix = sriov ? ' (SR-IOV)' : '';
  return `${formatRate(rate || 0)}${sriovSuffix}${iSuffix}`;
}

function reduceNodes(nodes: ReactNode[]): ReactNode {
  return nodes.reduce((all, elem) => all ? <>{all} + {elem}</> : elem, undefined);
}

function groupById<T>(items: T[], getId: (arg: T) => string, getElement?: (arg: T) => ReactNode): ReactNode[] {
  const itemsById: { [id: string]: { total: number, element: ReactNode } } = {};
  items.forEach(i => {
    const id = getId(i);
    itemsById[id] ||= { total: 0, element: getElement ? getElement(i) : id };
    itemsById[id].total += 1;
  });
  return Object.entries(itemsById).map(([id, { total, element }]) => (
    <span key={id}>
      {total === 1 ? element : <>{total} x {element}</>}
    </span>
  ));
}

function formatGPU(gpu: GPUDevice) {
  return <GPU device={gpu} />;
}

function Accelerators({ cluster }: { cluster: Cluster }) {
  const otherDevices: ReactNode[] = [];
  const nodes = Object.values(cluster.nodes);
  if (nodes.length > 0 && nodes[0].other_devices) {
    otherDevices.push(...groupById(Object.values(nodes[0].other_devices), formatOther));
  }

  // For now we assume only gpus might be heterogeneous accross nodes.
  // Create a structure with gpus
  const nodesByGPUs: { [id: string]: { nuids: number[], node: ClusterNode } } = {};

  // Group similar nodes based on their json export.
  Object.values(cluster.nodes).forEach(n => {
    const gpusForNode = groupGPUsById(n);
    if (n.gpu_devices) {
      Object.values(n.gpu_devices).forEach(gpu => {
        const gpuId = JSON.stringify(createGPUIdentifier(gpu));
        gpusForNode[gpuId] ||= { n: 0, gpu };
        gpusForNode[gpuId].n += 1;
      });
      const jsonForNode = JSON.stringify(
        Object.entries(gpusForNode).map(([ id, { n }]) => ({ id, n }))
      );
      nodesByGPUs[jsonForNode] ||= { nuids: [], node: n };
      nodesByGPUs[jsonForNode].nuids.push(nodeIdAsNumber(cluster, n.uid));
    }
  });

  const heterogeneous = Object.keys(nodesByGPUs).length > 1;
  const gpuDevices = Object.values(nodesByGPUs).map(({ nuids, node }, index) => {
    const nodesIntervals = computeIntervals(nuids);
    const gpuDevices = node.gpu_devices || {};
    return (
      <div key={index}>
        {heterogeneous && <span>{formatIntervals(nodesIntervals)}:</span>}
        {reduceNodes(groupById(Object.values(gpuDevices), n => JSON.stringify(createGPUIdentifier(n)), formatGPU))}
      </div>
    );
  });

  return (
    <>
      {reduceNodes(otherDevices.length > 0 ? otherDevices : [' '])}
      {...gpuDevices}
    </>
  );
}

function countAccelerators(node: ClusterNode): number {
  return Object.keys(node.gpu_devices || {}).length +
         Object.keys(node.other_devices || {}).length;
}

function getStorageSize(storage: Storage) {
  return formatSize(storage.size, 'metric', 1)
}

function formatStorage(storage: Storage): ReactNode {
  const { size, reservation } = storage;
  return (
    <>
      {formatSize(size, 'metric', 1)}
      {reservation && (
        <ExternalLink href={RESERVATION_URL}>*</ExternalLink>
      )}
    </>
  );
}

function secondary(storage: Storage) {
  return storage.id !== 'disk0';
}

function FormatStorages({ storages }: { storages: Storage[] }) {
  const others: ReactNode[] = groupById(storages.filter(secondary), getStorageSize, formatStorage);
  const primary = storages.filter(s => !secondary(s)).map(s => (
    <b key={s.id}>{formatStorage(s)}</b>
  ));
  const formatted: ReactNode[] = [];
  if (primary.length > 0) {
    formatted.push(...primary);
  }
  if (others.length > 0) {
    formatted.push(...others);
  }
  return reduceNodes(formatted);
}


function FormatAccessCondition({
  site, cluster
}: {
  site: string,
  cluster: Cluster,
}) {
  if (cluster.exotic) {
    return (
      <ExternalLink
        href={`https://www.grid5000.fr/w/Exotic#${capitalize(site)}:_${cluster.uid}`}
      >
        exotic
      </ExternalLink>
    );
  }
  if (cluster.queues.includes('testing')) {
    return <span><b>testing</b> queue</span>;
  }
  if (cluster.queues.includes('production')) {
    return (
      <>
        <ExternalLink
          href="https://www.grid5000.fr/w/Grid5000:UsagePolicy#Rules_for_the_production_queue"
        >
          production
        </ExternalLink>
        {' '}queue
      </>
    );
  }
  return '';
}

type FilterClusterF = (c: Cluster) => boolean;

function computeRowsFromRefrepo(refrepo: RefRepo, filterCluster: FilterClusterF): [Row[], boolean, boolean] {
  const selected = (c: Cluster) => Object.values(c.nodes).length > 0 && filterCluster(c);
  let hasReservableDisk = false;
  let hasUnsupportedGPU = false;
  return [Object.values(refrepo.sites).flatMap(s => {
    return Object.values(s.clusters).filter(selected).flatMap(c => {
      // FIXME: handle per-node GPU specificities.
      const node: ClusterNode = Object.values(c.nodes)[0];
      const ram = node.memory_devices.filter(isDram).reduce(sizeReducer, 0);
      const pmem = node.memory_devices.filter(isPmem).reduce(sizeReducer, 0);
      const totalStorage = node.storage_devices.reduce(sizeReducer, 0);
      const visibleNetworks = node.network_adapters.filter(visibleNetwork);
      const totalRate = visibleNetworks.reduce((sum, net) => sum + (net.rate || 0), 0);
      if (node.storage_devices.filter(val => val.reservation).length > 0) {
        hasReservableDisk = true;
      }
      const gpus = Object.values(node.gpu_devices || {});
      if (gpus.filter(gpu => !isSupported(gpu)).length > 0) {
        hasUnsupportedGPU = true;
      }
      return {
        id: { value: c.uid },
        site: {
          value: s.uid,
          element: <Link href={`/hardware/${s.uid}`}>{s.uid}</Link>,
        },
        cluster: {
          value: normalizeClusterName(c.uid),
          element: <Link href={`/hardware/${s.uid}/#${c.uid}`}>{c.uid}</Link>,
        },
        access: {
          value: s.uid,
          element: <FormatAccessCondition site={s.uid} cluster={c} />,
        },
        creation: { value: formatDate(c.created_at) },
        manufactured: { value: formatDate(c.manufactured_at) },
        nodes: { value: Object.values(c.nodes).length },
        nCPUs: { value: node.architecture.nb_procs },
        CPUName: { value: `${node.processor.model} ${node.processor.version}` },
        nCPUCores: { value: node.architecture.nb_cores },
        arch: { value: node.architecture.platform_type },
        memory: { value: ram + pmem, element: formatMem(ram, pmem, 'IEC', 0) },
        storage: {
          value: totalStorage,
          element: <FormatStorages storages={node.storage_devices} />,
        },
        network: {
          value: totalRate,
          element: reduceNodes(groupById(visibleNetworks, formatNetwork)),
        },
        accelerators: { value: countAccelerators(node), element: <Accelerators cluster={c} /> },
      };
    });
  }).sort(getComparator('asc', 'site')), hasReservableDisk, hasUnsupportedGPU];
}

export default function ClustersTable({
  refrepo,
  filterCluster,
  disableAccessCondition = false,
}: {
    refrepo: RefRepo,
    filterCluster?: FilterClusterF,
    disableAccessCondition?: boolean,
}) {
  const [rows, hasReservableDisk, hasUnsupportedGPU] = useMemo(
    () => computeRowsFromRefrepo(refrepo, filterCluster || (() => true)),
    [refrepo, filterCluster]);
  const headers: Header[] = useMemo(() => {
    const localHeaders = disableAccessCondition ?
      HEADERS.filter(h => (h as BasicHeader).field !== 'access') : HEADERS;
    return Object.keys(refrepo.sites).length > 1 ? localHeaders : localHeaders.slice(1);
  }, [refrepo, disableAccessCondition]);
  // FIXME: group cpu columns together
  return (
    <>
      <DataTable
        rows={rows}
        columns={headers}
      />
      {hasReservableDisk && (
        <div>
          <i>*: disk is <ExternalLink href={RESERVATION_URL}>reservable</ExternalLink></i>
        </div>
      )}
      {hasUnsupportedGPU && (
        <div>
          <i>**: crossed GPUs are not supported in the default environment</i>
        </div>
      )}
    </>
  );
}

import { Cluster } from "@grid5000/refrepo-ts";
import ExternalLink from '@/components/ExternalLink';
import { capitalize } from "@/lib/table";

export default function ExoticLink({ site, cluster }: {site: string, cluster: Cluster }) {
  const clusterLink = `https://www.grid5000.fr/w/Exotic#${capitalize(site)}:_${cluster.uid}`;
  return (
    <ExternalLink href={clusterLink}>
      exotic
    </ExternalLink>
  );
}

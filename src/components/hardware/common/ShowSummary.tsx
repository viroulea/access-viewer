import { Summary, formatFlops, formatMem, formatSize } from "@/lib/hardware";

import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardHeader from "@mui/material/CardHeader";

function disksString(ssds: number, hdds: number, storage: number) {
  let disks = [];
  if (ssds > 0) {
    disks.push(`${ssds} SSDs`);
  }
  if (hdds > 0) {
    disks.push(`${hdds} HDDs`);
  }
  return `${disks.join(' and ')} on nodes (total: ${formatSize(storage, 'metric')})`;
}

export default function ShowSummary({
  title, summary
}: { summary: Summary, title: string }) {
  const {
    nSites, nClusters, nNodes, nCores, nGPUs, nGPUsCores,
    ram, pmem, ssds, hdds, storage, flops,
  } = summary;
  return (
    <Card variant="outlined">
      <CardHeader title={title} />
      <CardContent>
        <ul>
          {nSites > 1 && (
            <li>{nSites} sites</li>
          )}
          <li>{nClusters} clusters</li>
          <li>{nNodes} nodes</li>
          <li>{nCores} CPU cores</li>
          {nGPUs > 0 && (
            <>
              <li>{nGPUs} GPUs</li>
              <li>{nGPUsCores} GPUs cores</li>
            </>
          )}
          <li>{formatMem(ram, pmem)}</li>
          <li>{disksString(ssds, hdds, storage)}</li>
          <li>{formatFlops(flops)} (excluding GPUs)</li>
        </ul>
      </CardContent>
    </Card>
  );
}

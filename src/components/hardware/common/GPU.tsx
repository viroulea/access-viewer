import { formatGPUAsString, isSupported } from "@/lib/hardware";
import { GPUDevice } from "@grid5000/refrepo-ts";

export default function GPU({
  device,
  includeCC
} : {
  device: GPUDevice, includeCC?: boolean
}) {
  const { vendor, model, memory, compute_capability } = device;
  const gpuString = formatGPUAsString(device);
  const gpuElement = (
    <>
      {gpuString}
      {includeCC && compute_capability && (
        <>
          <br/>
          Compute capability: {compute_capability}
        </>
      )}
    </>
  );
  return isSupported(device) ? gpuElement : <><s>{gpuElement}</s>**</>;
}

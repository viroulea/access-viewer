import { Cluster, RefRepo, normalizeClusterName } from "@grid5000/refrepo-ts";
import { Summary, computeSummary, isFirst } from "@/lib/hardware";

import Alert from "@mui/material/Alert";
import Box from "@mui/material/Box";
import ClustersTable from "@/components/hardware/common/ClustersTable";
import Container from "@mui/material/Container";
import Divider from "@mui/material/Divider";
import Grid from "@mui/material/Unstable_Grid2";
import Link from "next/link";
import MUILink from "@mui/material/Link";
import ShowClusterNodes from "@/components/hardware/site/ShowClusterNodes";
import ShowSummary from "@/components/hardware/common/ShowSummary";
import Toc from "@/components/Toc";
import Typography from "@mui/material/Typography";
import { useMemo } from "react";

function titleForSite(site: string) {
  return `Hardware summary for ${site}`;
}

function titleForQueue(queue: string) {
  return `${queue} queue resources`;
}

// 'createFilter' is a function that returns a filtering function specifically
// for the given (optional) queue, among the clusters of the current site.
function createFilter(clustersUids: string[], queue?: string) {
  return ({ uid, queues }: Cluster) => {
    return clustersUids.includes(uid) && (!queue || queues.includes(queue));
  };
}

function sortNormalize(a: Cluster, b: Cluster): number {
  const normA = normalizeClusterName(a.uid);
  const normB = normalizeClusterName(b.uid);
  if (normA < normB) {
    return -1;
  }
  if (normA > normB) {
    return 1;
  }
  return 0;
}

export default async function ShowSite({
  site,
  refrepo,
} : { site: string, refrepo: RefRepo }) {
  const clusters = Object.values(refrepo.sites[site].clusters);
  const clustersUids = clusters.map(({ uid }) => uid);

  const filterClusterInSite = useMemo(() => createFilter(clustersUids), [clustersUids]);
  const filterClusterInQueue = useMemo(() =>
    (q: string) => createFilter(clustersUids, q),
  [clustersUids]);

  const summary = useMemo(() =>
    computeSummary(refrepo, filterClusterInSite),
  [refrepo, filterClusterInSite]);
  const summaryPerQueue = useMemo(() => {
    let ret : { [id: string]: Summary } = {};
    // Compute the list of unique queues, exclude admin queue, and then the
    // summary for all of them.
    clusters.flatMap(({ queues }) => queues)
      .filter(isFirst)
      .filter(q => 'admin' !== q)
      .forEach(q => {
        ret[q] = computeSummary(refrepo, filterClusterInQueue(q));
      });
    return ret;
  }, [refrepo, clusters, filterClusterInQueue]);

  const allQueues = Object.keys(summaryPerQueue).sort();
  const queuesForSummary = allQueues.filter(q => q !== 'testing');
  const totalColumns = (queuesForSummary.length > 1 ? queuesForSummary.length : 0) + 1;

  const tocEntries = useMemo(() => {
    return [
      {
        element: 'Summary',
        anchor: 'summary',
        subEntries: queuesForSummary.map(q => ({
          element: `${q} queue resources`,
          anchor: `summary-${q}`,
        })),
      },
      {
        element: 'Clusters summary',
        anchor: 'clusters-summary',
        subEntries: allQueues.map(q => ({
          element: `${q} queue resources`,
          anchor: `clusters-summary-${q}`,
        })),
      },
      ...allQueues.map(q => ({
        element: `Clusters in the ${q} queue`,
        anchor: `clusters-in-${q}`,
        subEntries: clusters.filter(filterClusterInQueue(q)).sort(sortNormalize).map(({ uid }) => ({
          element: uid,
          anchor: uid,
        })),
      })),
    ];
  }, [allQueues, queuesForSummary, clusters, filterClusterInQueue]);

  return (
    <Container maxWidth="xl">
      <Toc entries={tocEntries} />
      <Typography variant="h4" gutterBottom id="summary">Summary</Typography>
      <Divider sx={{mb: 2}}/>
      <Grid container spacing={2} sx={{mb: 2}} columns={totalColumns}>
        <Grid xs={totalColumns} md={1}>
          <ShowSummary summary={summary} title={titleForSite(site)} />
        </Grid>
        {queuesForSummary.length > 1 && (
          queuesForSummary.map(q => (
            <Grid xs={totalColumns} md={1} key={q} id={`summary-${q}`}>
              <ShowSummary summary={summaryPerQueue[q]} title={titleForQueue(q)} />
            </Grid>
          ))
        )}
      </Grid>
      <Alert severity="info" sx={{mb: 2}}>
        You can browse, see your access level, and (soon) see available resources for this site{' '}
        <MUILink component={Link} href={`/browse/?sites=${site}`}>
          here
        </MUILink>.
      </Alert>
      <Typography variant="h4" gutterBottom id="clusters-summary">
        Clusters summary
      </Typography>
      <Divider sx={{mb: 2}}/>
      {allQueues.map(q => {
        return (
          <Box sx={{mb: 2}} key={q}>
            <Typography variant="h5" id={`clusters-summary-${q}`}>
              {q} queue resources
            </Typography>
            <Divider sx={{mb: 2}}/>
            <ClustersTable
              refrepo={refrepo}
              filterCluster={filterClusterInQueue(q)}
              disableAccessCondition={['production', 'testing'].includes(q)}
            />
          </Box>
        );
      })}
      {allQueues.map(q => {
        return (
          <Box sx={{mb: 2}} key={q}>
            <Typography variant="h4" id={`clusters-in-${q}`}>
              Clusters in the {q} queue
            </Typography>
            <Divider sx={{mb: 2}}/>
            {clusters.filter(filterClusterInQueue(q)).sort(sortNormalize).map(c => (
              <ShowClusterNodes
                cluster={c}
                site={site}
                key={c.uid}
              />
            ))}
          </Box>
        );
      })}
    </Container>
  );
}

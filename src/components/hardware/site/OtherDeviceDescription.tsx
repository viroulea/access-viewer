import { OtherDevice } from "@grid5000/refrepo-ts";

export default function OtherDevicesDesc({ dev }: { dev: OtherDevice }) {
  const { count, vendor, model } = dev;
  return (
    <div>
      {count > 1 && `${count} x `}
      {vendor} {model}
    </div>
  );
}

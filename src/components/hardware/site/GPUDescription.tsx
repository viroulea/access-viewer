import { GPUById, isSupported } from "@/lib/hardware";
import GPU from "@/components/hardware/common/GPU";

export default function GPUDescription({ gpus }: { gpus: GPUById }) {
  return Object.values(gpus).map(({ n, gpu }, index) => (
    <div key={index}>
      {n > 1 && `${n} x `}
      <GPU device={gpu} includeCC />
      {!isSupported(gpu) && (
        <div><i>not supported by the default environment</i></div>
      )}
    </div>
  ));
}

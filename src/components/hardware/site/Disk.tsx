import { formatSize, isPrimary } from "@/lib/hardware";
import ExternalLink from '@/components/ExternalLink';
import { Storage } from "@grid5000/refrepo-ts";

export default function Disk({ disk }: { disk: Storage }) {
  const { id, model, size, storage, vendor, reservation } = disk;
  const primaryString = isPrimary(disk) ? ' (primary disk)' : '';
  const desc = `${formatSize(size, 'metric', 0)} ${storage} ${disk.interface}`;
  const vendorString = `${vendor} ${model} `;
  return (
    <>
      {id}, {desc} {vendorString}
      (<code>/dev/{id}</code>)
      {primaryString}
      {' '}
      {reservation && (
        <ExternalLink href="https://www.grid5000.fr/w/Disk_reservation">
          (reservable)
        </ExternalLink>
      )}
    </>
  );
}

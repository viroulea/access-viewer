import ExternalLink from '@/components/ExternalLink';
import { Network } from "@grid5000/refrepo-ts";
import { ReactNode } from "react";
import Typography from "@mui/material/Typography";
import { formatRate } from "@/lib/hardware";

export default function NetworkDevice({ net, index }: { net: Network, index: number }) {
  const {
    device, driver, kavlan, rate, name, model, mountable, mounted, sriov, sriov_totalvfs, vendor
  } = net;
  const fullName = name == device ? name : `${device}/${name}`;
  const rateStr = rate ? `, configured rate: ${formatRate(rate)}` : '';
  const modelStr = `, model: ${model && vendor ? `${vendor} ${model}` : 'N/A'}`;
  const driverStr = `, driver: ${driver}`;
  let suffix: ReactNode = '';
  const hasKaVLAN = kavlan && net.interface == 'Ethernet';
  const hasNoKaVLAN = !kavlan && net.interface == 'Ethernet';
  if (!mountable) {
    suffix = '- unavailable for experiment';
  } else if (net.interface == 'Ethernet') {
    if (!kavlan) {
      suffix = '- no KaVLAN';
    } else if (index > 1) {
      suffix = (
        <ExternalLink
          href="https://www.grid5000.fr/w/Advanced_KaVLAN#A_simple_multi_NICs_example"
        >
          (multi NICs example)
        </ExternalLink>
      );
    }
  }
  const typoColor = mountable ? "text.primary" : "text.secondary";
  return (
    <Typography color={typoColor}>
      {fullName}, {net.interface}{rateStr}{modelStr}{driverStr}
      {' '}
      {suffix}
    </Typography>
  );
}


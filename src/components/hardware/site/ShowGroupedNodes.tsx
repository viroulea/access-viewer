import { Cluster, GPUDevice } from "@grid5000/refrepo-ts";
import { SimilarNodesDescription, computeIntervals, formatDate, formatIntervals, formatMem, formatSize, groupGPUsById, isDram, isPmem, sizeReducer } from "@/lib/hardware";

import Box from "@mui/material/Box";
import Disk from "./Disk";
import ExoticLink from "@/components/hardware/common/ExoticLink";
import ExternalLink from '@/components/ExternalLink';
import GPUDescription from "./GPUDescription";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import NetworkDevice from "@/components/hardware/site/NetworkDevice";
import OtherDevicesDescription from "@/components/hardware/site/OtherDeviceDescription";
import { ReactNode } from "react";
import Typography from "@mui/material/Typography";

function gpuId(gpu: GPUDevice) {
  const { vendor, model, memory } = gpu;
  return `${vendor}-${model}-${formatSize(memory, 'IEC', 0)}`;
}

function FormatAccessCondition(site: string, cluster: Cluster): ReactNode {
  if (cluster.exotic) {
    return (
      <>
        <ExoticLink site={site} cluster={cluster}/ > job type
      </>
    );
  }
  if (cluster.queues.includes('testing')) {
    return <span><b>testing</b> queue</span>;
  }
  if (cluster.queues.includes('production')) {
    return (
      <>
        <ExternalLink
          href="https://www.grid5000.fr/w/Grid5000:UsagePolicy#Rules_for_the_production_queue"
        >
          production
        </ExternalLink>
        {' '}queue
      </>
    );
  }
  return '';

}

function Info({
  label,
  children,
}: { label: string, children: ReactNode }) {
  return (
    <Box sx={{display: 'flex'}}>
      <Box sx={{flex: 1}}>
        <b>{label}</b>
      </Box>
      <Box sx={{flex: 5}}>
        {children}
      </Box>
    </Box>
  );
}

export default function ShowGroupedNodes({
  cluster, nodes, site, splitted,
}: {
  cluster: Cluster,
  nodes: SimilarNodesDescription,
  site: string,
  splitted: boolean,
}) {
  const { uid, exotic, model, manufactured_at, created_at } = cluster;
  const node = nodes.node;
  const nNodes = nodes.ids.length;
  const cpuPerNode = node.architecture.nb_procs;
  const coresPerNode = node.architecture.nb_cores;
  const coresPerCPU = coresPerNode / cpuPerNode;
  const cpus = cpuPerNode * nNodes;
  const cores = coresPerNode * nNodes;
  const ram = node.memory_devices.filter(isDram).reduce(sizeReducer, 0);
  const pmem = node.memory_devices.filter(isPmem).reduce(sizeReducer, 0);
  const publicNetAdapters = node.network_adapters.filter(net => !net.management);
  let multiNICIndex = 0;
  const gpuById = groupGPUsById(node);
  const nodesIntervals = computeIntervals(nodes.ids);
  const AccessCondition = FormatAccessCondition(site, cluster);
  return (
    <>
      {splitted && (
        <Typography variant="h6" gutterBottom>
          {uid}-{formatIntervals(nodesIntervals)} ({nNodes} nodes, {cpus} CPUS, {cores} cores)
        </Typography>
      )}
      {AccessCondition !== '' && (
        <Info label="Access condition:">
          {AccessCondition}
        </Info>
      )}
      <Info label="Model:">
        {model}
      </Info>
      <Info label="Manufacturing date:">
        {formatDate(manufactured_at)}
      </Info>
      <Info label="Date of arrival:">
        {formatDate(created_at)}
      </Info>
      <Info label="CPU:">
        {node.processor.other_description}, {cpuPerNode} CPUs/node, {coresPerCPU} cores/CPU
      </Info>
      <Info label="Memory:">
        {formatMem(ram, pmem, 'IEC', 0)}
      </Info>
      <Info label="Storage:">
        <List sx={{listStyleType: 'disc', pl: 2}}>
          {node.storage_devices.map(disk => (
            <ListItem key={disk.id} sx={{display: 'list-item', py: 0}}>
              <Disk disk={disk} />
            </ListItem>
          ))}
        </List>
      </Info>
      <Info label="Networks:">
        <List sx={{listStyleType: 'disc', pl: 2}}>
          {publicNetAdapters.map(net => {
            if (net.device.startsWith('eth')) {
              multiNICIndex += 1;
            }
            return (
              <ListItem key={net.device} sx={{display: 'list-item', py: 0}}>
                <NetworkDevice net={net} index={multiNICIndex} />
              </ListItem>
            )})}
        </List>
      </Info>
      {node.gpu_devices && (
        <Info label="GPU:">
          <GPUDescription gpus={gpuById} />
        </Info>
      )}
      {node.other_devices && (
        <Info label="FPGA:">
          {Object.values(node.other_devices).map((dev, index) => (
            <OtherDevicesDescription dev={dev} key={index} />
          ))}
        </Info>
      )}
      {exotic && (
        <Info label="Note:">
          This cluster is defined as exotic. Please read the
          {' '}<ExoticLink site={site} cluster={cluster} />{' '}
          page for more information.
        </Info>
      )}
    </>
  );
}


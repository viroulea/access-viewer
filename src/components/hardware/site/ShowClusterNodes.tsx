import { Cluster, ClusterNode } from "@grid5000/refrepo-ts";
import { computeReservation, createNodeIdentifier, nodeIdAsNumber } from "@/lib/hardware";
import Box from "@mui/material/Box";
import Divider from "@mui/material/Divider";
import ExternalLink from '@/components/ExternalLink';
import Link from '@/components/InternalLink';
import ShowGroupedNodes from "@/components/hardware/site/ShowGroupedNodes";
import Typography from "@mui/material/Typography";
import { ganttUrl } from "@/lib/routes";
import styles from "@/components/Links.module.css";

export default function ShowClusterNodes({
  cluster,
  site,
}: { cluster: Cluster, site: string }) {
  const { uid, nodes, model, manufactured_at, created_at } = cluster;
  const nodeByJson: { [json: string]: { nuids: string[], node: ClusterNode } } = {};

  // Group similar nodes based on their json export.
  Object.values(nodes).forEach(n => {
    const json = JSON.stringify(createNodeIdentifier(n));
    nodeByJson[json] ||= { nuids: [], node: n };
    nodeByJson[json].nuids.push(n.uid);
  });

  const nodeIdAsNumberForCluster = nodeIdAsNumber.bind(null, cluster);
  const descriptions = Object.values(nodeByJson).map(({ nuids, node }) => {
    return {
      ids: nuids.map(nodeIdAsNumberForCluster),
      node,
    };
  }).sort((a, b) => a.ids[0] - b.ids[0]);

  const splitted = descriptions.length > 1;

  const jsonUrl = `https://public-api.grid5000.fr/stable/sites/${site}/clusters/${uid}/nodes.json?pretty=1`;

  const { cpus, cores }: { cpus: number, cores: number } =
    Object.values(nodes).reduce((acc, node) => {
      acc.cpus += node.architecture.nb_procs;
      acc.cores += node.architecture.nb_cores;
      return acc;
    }, { cpus: 0, cores: 0 });

  return (
    <>
      <Typography variant="h4" gutterBottom id={uid}>
        <Link href={`#${cluster.uid}`} className={styles.headerLink}>
          {cluster.uid}
        </Link>
      </Typography>
      <Divider />
      <Typography sx={{ fontWeight: 'bold' }} gutterBottom>
        {Object.keys(nodes).length} nodes, {cpus} CPUS, {cores} cores
        {splitted && (
          <Typography component='span'>
            , split as follows due to differences between nodes
          </Typography>
        )}
        {' ('}
        <ExternalLink href={jsonUrl}>
          json
        </ExternalLink>
        {', '}
        <ExternalLink href={ganttUrl(site, cluster.queues, cluster.uid)}>
          drawgantt
        </ExternalLink>).
      </Typography>
      <Typography sx={{ fontWeight: 'bold' }}>
        Reservation example:
      </Typography>
      <Box sx={{mb: 2}}>
        <pre>
          f{site}:~$ {computeReservation(site, cluster)}
        </pre>
      </Box>
      {descriptions.map((nodes, index) =>
        <Box sx={{mb: 3}} key={index}>
          <ShowGroupedNodes
            nodes={nodes}
            cluster={cluster}
            site={site}
            splitted={splitted}
          />
        </Box>
      )}
    </>
  );
}

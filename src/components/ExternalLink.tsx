import Link, { LinkProps } from "@mui/material/Link";

export default function ExternalLink(props: LinkProps) {
  return <Link rel="noreferrer" target="_blank" {...props} />;
}

import MUILink, { LinkProps } from "@mui/material/Link";
import Link from "next/link";

export default function InternalLink(props: LinkProps) {
  return <MUILink component={Link} {...props} />;
}

"use client";

import AppBar from '@mui/material/AppBar';
import CircularProgress from '@mui/material/CircularProgress';
import HomeIcon from '@mui/icons-material/Home';
import IconButton from '@mui/material/IconButton';
import Link from 'next/link';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { UserData } from '@/lib/user';
import useUser from '@/hooks/useUser';

function textForUser(user: UserData | undefined) {
  if (!user) {
    return 'Not authenticated'
  }
  const { first_name, last_name, gga } = user;
  const ggaAsString = gga.length === 0 ? 'no gga' : gga.join(', ');
  return `${first_name} ${last_name} (${ggaAsString})`;
}

function AutoUserSignIn() {
  const { user, isLoading } = useUser();
  return (
    <>
      {isLoading ? (
        <CircularProgress color="inherit" />
      ) : (
        <Typography variant="h6">
          {textForUser(user)}
        </Typography>
      )}
    </>
  );
}

// Grid5000 has an http-based authentication, which really messes up how robots
// can see the pages according to some SEO tools.
// Until we migrate to a more modern authentication method, the `autoSignIn`
// parameter allows us to use this http authentication for pages where SEO
// doesn't matter (ie: the browse page), while keeping a clean app bar for
// pages where we'd like proper referencing (ie: hardware/statically generated
// pages).
export default function ResourcesExplorerAppBar({ autoSignIn }: {
  autoSignIn?: boolean;
}) {
  const { user, isLoading } = useUser();
  return (
    <AppBar position="static">
      <Toolbar>
        <IconButton aria-label="home" component={Link} href="/">
          <HomeIcon />
        </IconButton>
        <Typography variant="h6" sx={{ flexGrow: 1 }}>
          Resources Explorer
        </Typography>
        {autoSignIn && (
          <AutoUserSignIn />
        )}
      </Toolbar>
    </AppBar>
  );
}

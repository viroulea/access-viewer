import { Accesses, accessLabelToValue, accessValueToNumber, accessValueToString } from "@/lib/accesses";
import { Cluster, ClusterNode, RefRepo, Site, normalizeClusterName } from "@grid5000/refrepo-ts";
import { DataGrid, GridColDef, GridComparatorFn, GridFilterModel, GridRenderCellParams, GridRowModel, GridRowsProp, GridToolbarColumnsButton, GridToolbarContainer, GridToolbarProps, ToolbarPropsOverrides, gridStringOrNumberComparator } from "@mui/x-data-grid";
import { Dispatch, JSXElementConstructor, useMemo, useState } from "react";
import JobsCell, { JobCellType } from "./JobsCell";
import { lighten, styled } from '@mui/material/styles';
import { useArrayStringParamValue, useStringParamValue } from "@/hooks/useParamValue";
import useJobsData, { JobsData } from "@/hooks/useJobsData";

import Box from "@mui/material/Box";
import { GGA } from "@/lib/ggas";
import ItemsSelector from "./ItemsSelector";
import NodesPopover from "@/components/browse/NodesPopover";
import TextField from "@mui/material/TextField";
import { accessesUrl } from "@/lib/routes";
import { defaultFetch } from "@/lib/request";
import useRefrepo from '@grid5000/refrepo-ts/useRefrepo';
import useSWR from "swr";

type ComparatorValue = {
  originalValue: string,
  clusterName: string,
};

const ABACA = 'Abaca';
const SLICES = 'SLICES-FR';
const PLATFORMS = [ABACA, SLICES];

const clusterNameComparator: GridComparatorFn<string> = (a, b, param1, param2) => {
  return gridStringOrNumberComparator(normalizeClusterName(a),
    normalizeClusterName(b),
    param1,
    param2);
}

const siteNameComparator: GridComparatorFn = (a, b, param1, param2) => {
  const siteComp = gridStringOrNumberComparator(a.originalValue,
    b.originalValue,
    param1,
    param2);
  if (siteComp === 0) {
    return clusterNameComparator(a.clusterName, b.clusterName, param1, param2);
  } else {
    return siteComp;
  }
}

const accessComparator: GridComparatorFn = (a, b, param1, param2) => {
  const accessValue =
    accessValueToNumber(a.originalValue) - accessValueToNumber(b.originalValue);
  if (accessValue === 0) {
    return clusterNameComparator(a.clusterName, b.clusterName, param1, param2);
  } else {
    return accessValue;
  }
};

const nodesComparator: GridComparatorFn = (a, b, param1, param2) => {
  return gridStringOrNumberComparator(a.length, b.length, param1, param2);
};

const headers: GridColDef[] = [
  {
    field: 'site',
    headerName: 'Site',
    sortComparator: siteNameComparator,
    valueGetter: (value, row): ComparatorValue => ({
      originalValue: value,
      clusterName: row.cluster,
    }),
    valueFormatter: (value: ComparatorValue) => {
      return value.originalValue;
    },
    flex: 1,
  },
  {
    field: 'cluster',
    headerName: 'Cluster',
    sortComparator: clusterNameComparator,
    flex: 1,
  },
  {
    field: 'nodes',
    headerName: 'Nodes',
    renderCell: (params: GridRenderCellParams<any, string[]>) => (
      <NodesPopover nodes={params.value || []} />
    ),
    sortComparator: nodesComparator,
    flex: 1,
  },
  {
    field: 'jobs',
    headerName: 'Jobs',
    renderCell: (params: GridRenderCellParams<any, JobCellType>) => {
      if (params?.value) {
        return <JobsCell jobsData={params.value} />
      }
      return '';
    },
    flex: 1,
  },
  {
    field: 'access',
    headerName: 'Access Level',
    renderCell: (params: GridRenderCellParams<any, ComparatorValue>) => (
      <span>{accessValueToString(params.value?.originalValue)}</span>
    ),
    sortComparator: accessComparator,
    valueGetter: (value, row): ComparatorValue => ({
      originalValue: value,
      clusterName: row.cluster,
    }),
    // NOTE: this is required for the quick search to find "P1" and al.
    valueFormatter: (value: ComparatorValue) => {
      return value.originalValue;
    },
    flex: 1,
  },
  { field: 'proc', headerName: 'Processor(s) per node', flex: 3 },
  { field: 'gpu', headerName: 'GPU(s) per node', flex: 3 },
];

function getStylesFromBaseColor(color: string) {
  return {
    backgroundColor: lighten(color, 0.3),
    '&:hover': {
      backgroundColor: lighten(color, 0.2)
    },
    '&.Mui-selected': {
      backgroundColor: lighten(color, 0.2),
      '&:hover': {
        backgroundColor: lighten(color, 0.1)
      },
    },
  };
}

const StyledDataGrid = styled(DataGrid)(({ theme }) => ({
  '& .access-noaccess': getStylesFromBaseColor(theme.palette.text.secondary),
  '& .access-p1': getStylesFromBaseColor(theme.palette.success.main),
  '& .access-p2': getStylesFromBaseColor(lighten(theme.palette.success.main, 0.2)),
  '& .access-p3': getStylesFromBaseColor(lighten(theme.palette.success.main, 0.4)),
  '& .access-p4': getStylesFromBaseColor(lighten(theme.palette.success.main, 0.6)),
}));

function rowFromAttributes(
  site: Site,
  cluster: Cluster,
  node: ClusterNode,
  group: GGA,
  allAccesses: Accesses | undefined,
  jobsData: JobsData,
): GridRowModel {
  const gpus = node.gpu_devices ? Object.values(node.gpu_devices) : [];
  const gpuDesc = gpus.length ? `${gpus.length} x ${gpus[0].model}` : '';
  const access = allAccesses ?
    accessLabelToValue(allAccesses[group.name][node.nodeset]?.label || 'default',
      group.access_level) :
    '...';
  const { perCluster, errors, loading } = jobsData;
  return {
    id: node.uid,
    site: site.uid,
    cluster: cluster.uid,
    jobs: {
      loading: loading,
      jobs: perCluster[cluster.uid],
      error: errors[site.uid],
      site: site.uid,
      cluster: cluster,
    },
    node: node.uid,
    proc: `${node.architecture.nb_procs} x ${node.processor.other_description}`,
    gpu: gpuDesc,
    nodeset: node.nodeset,
    platform: node.supported_job_types.queues.includes("production") ? ABACA : node.supported_job_types.queues.includes("default") ? SLICES : "Unknown",
    access,
  }
}

// This selects nodes which are not retired, until I fix the generated refrepo.
function activeNode(node: ClusterNode) {
  return node.architecture !== undefined;
}

function computeRowsFromGGA(gga: GGA,
  allAccesses: Accesses | undefined,
  refrepo: RefRepo,
  jobsData: JobsData) : GridRowsProp {
  const rowsByNodeset: { [nodeset: string]: GridRowModel } = {};
  // First compute the node rows per nodeset.
  Object.values(refrepo.sites).flatMap(
    site => Object.values(site.clusters).flatMap(
      cluster => Object.values(cluster.nodes).filter(activeNode).flatMap(node => {
        rowsByNodeset[node.nodeset] ||= [];
        rowsByNodeset[node.nodeset].push(rowFromAttributes(
          site, cluster, node, gga, allAccesses, jobsData
        ));
      })));

  // Then flatten the rows to have a single row per nodeset but with
  // the information about all the nodes.
  return Object.keys(rowsByNodeset).flatMap(key => {
    const row = rowsByNodeset[key][0];
    row['nodes'] = rowsByNodeset[key].flatMap((r: GridRowModel) => r.node);
    return row;
  });
}

function CustomToolbar({ search, setSearch, sites, setSites, allSites, platforms, setPlatforms } : {
  search: string,
  setSearch: Dispatch<string>,
  sites: Array<string>,
  setSites: Dispatch<Array<string>>,
  allSites: Array<string>,
  platforms: Array<string>,
  setPlatforms: Dispatch<Array<string>>,
}) {
  return (
    <GridToolbarContainer>
      <TextField
        id="outlined-basic"
        label="Quick search"
        variant="standard"
        sx={{ ml: 1 }}
        value={search}
        helperText="Try 'rennes a40' or 'p1'"
        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
          setSearch(event.target.value);
        }}
      />
      <Box sx={{ flexGrow: 1 }} />
      <ItemsSelector
        items={platforms}
        setItems={setPlatforms}
        allItems={PLATFORMS}
        labelItems="Platform"
      />
      <Box sx={{ flexGrow: 1 }} />
      <ItemsSelector
        items={sites}
        setItems={setSites}
        allItems={allSites}
        labelItems="site"
      />
      <GridToolbarColumnsButton />
    </GridToolbarContainer>
  );
}

type ToolbarType = JSXElementConstructor<GridToolbarProps & ToolbarPropsOverrides>;

const EMPTY_REFREPO: RefRepo = { sites: {} };

function getDefaultPlatforms(group: GGA): string[] {
  return group.site.startsWith("slices-fr") ? [SLICES] :  group.site.startsWith("mc") ? [ABACA] : [];
}

export default function ResourcesTable({ group, refrepoBranch }: {
  group: GGA,
  refrepoBranch: string,
}) {
  // We absolutely do want useSWR and not useSWRImmutable here: while we don't
  // expect the refrepo to change during one browse session, using the immutable
  // hook also means we rely only on the fallback data, which might be outdated!
  const { refrepo: remoteRefRepo } = useRefrepo(useSWR, refrepoBranch);
  const refrepo: RefRepo = remoteRefRepo || EMPTY_REFREPO;
  const allSites = useMemo(
    () => Object.keys(refrepo.sites).sort(),
    [refrepo.sites]);

  const jobsData: JobsData = useJobsData(allSites);

  const { data: allAccesses } = useSWR(accessesUrl(), defaultFetch);

  const allClusters = useMemo(() =>
    computeRowsFromGGA(group, allAccesses, refrepo, jobsData)
  , [group, refrepo, jobsData, allAccesses]);

  const [search, setSearch, debouncedSearch] = useStringParamValue('search', '');

  const [platforms, setPlatforms] = useArrayStringParamValue(
    'platforms', getDefaultPlatforms(group));

  const [prevGroup, setPrevGroup] = useState<GGA>(group);
  if (group !== prevGroup) {
    setPrevGroup(group);
    setPlatforms(getDefaultPlatforms(group));
  }

  const [sites, setSites] = useArrayStringParamValue(
    'sites',
    []);

  const filteredRows = useMemo(
    () => allClusters
      .filter(val => ((sites.length ===0 || sites.includes(val.site)) && (platforms.length ===0 || platforms.includes(val.platform)))),
    [allClusters, sites, platforms]);

  const filterModel: GridFilterModel = {
    items: [],
    quickFilterValues: debouncedSearch.split(' '),
  };

  // FIXME: try to figure out the weird alignment issue
  // https://codesandbox.io/p/sandbox/serene-keller-qgquno?file=%2Fsrc%2FApp.js%3A51%2C2-51%2C70

  return (
    <>
      <StyledDataGrid
        initialState={{
          density: 'compact',
          sorting: {
            sortModel: [{ field: 'access', sort: 'asc' }],
          },
        }}
        // This overrides the default desc, asc, null to remove the "unsort"
        // behavior.
        sortingOrder={['asc', 'desc']}
        rows={filteredRows}
        columns={headers}
        filterModel={filterModel}
        disableColumnFilter
        ignoreDiacritics
        slots={{
          toolbar: CustomToolbar as ToolbarType
        }}
        slotProps={{
          toolbar: {
            search,
            setSearch,
            sites,
            setSites,
            allSites,
            platforms,
            setPlatforms,
          },
        }}
        getRowClassName={(params) => `access-${params.row.access}`}
      />
    </>
  )
}

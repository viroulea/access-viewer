import { Cluster, Job } from "@grid5000/refrepo-ts";

import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import CircularProgress from "@mui/material/CircularProgress";
import CloseIcon from '@mui/icons-material/Close';
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import ErrorIcon from '@mui/icons-material/Error';
import ExternalLink from "../ExternalLink";
import IconButton from "@mui/material/IconButton";
import InternalLink from '@/components/InternalLink';
import JobCard from "../JobCard";
import Link from "@mui/material/Link";
import { RequestError } from "@/lib/request";
import Stack from "@mui/material/Stack";
import Tooltip from "@mui/material/Tooltip";
import { ganttUrl } from "@/lib/routes";
import useMediaQuery from '@mui/material/useMediaQuery';
import { useState } from "react";
import { useTheme } from '@mui/material/styles';

export type JobCellType = {
  loading: boolean,
  jobs?: Job[],
  error?: RequestError,
  site: string,
  cluster: Cluster,
};

export default function JobsCell({ jobsData } : { jobsData: JobCellType }) {
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('md'));
  const { cluster, error, loading, jobs, site } = jobsData;
  const { uid, queues } = cluster;
  if (loading) {
    return <CircularProgress size="30px" />
  }
  if (error) {
    return (
      <Tooltip title={error.message}>
        <IconButton size="small">
          <ErrorIcon fontSize="inherit" />
        </IconButton>
      </Tooltip>
    );
  }

  if (jobs === undefined) {
    return 'None';
  }

  return (
    <div>
      <Link
        onClick={handleOpen}
        component="button"
        sx={{ color: 'black', textDecoration: 'underline' }}
      >
        {jobs.length} jobs
      </Link>
      <Dialog
        open={open}
        onClose={handleClose}
        maxWidth="xl"
        fullWidth
        fullScreen={fullScreen}
      >
        <DialogTitle
          sx={{display: 'flex', justifyContent: 'space-between'}}
        >
          Jobs running on {uid}
          <IconButton
            color="inherit"
            onClick={handleClose}
            aria-label="close"
          >
            <CloseIcon />
          </IconButton>
        </DialogTitle>
        <DialogContent>
          <Stack direction="column" spacing={2}>
            <Box>
              <Button
                variant="contained"
                href={`/hardware/${site}/#${uid}`}
                component={InternalLink}
                sx={{ mr: 1 }}
              >
                Hardware
              </Button>
              <Button
                rel="noreferrer"
                target="_blank"
                variant="contained"
                href={ganttUrl(site, queues, uid)}
                component={ExternalLink}
              >
                Drawgantt
              </Button>
            </Box>
            {jobs.map(job => (
              <JobCard job={job} key={job.uid} site={site} />
            ))}
          </Stack>
        </DialogContent>
      </Dialog>
    </div>
  );
}


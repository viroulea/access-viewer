"use client";
import SelectGroup, { GroupType } from '@/components/inputs/SelectGroup';
import { StringToT, useParamValue } from '@/hooks/useParamValue';
import { Suspense, useEffect, useMemo } from 'react';

import Alert from '@mui/material/Alert';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import { GGA } from '@/lib/ggas';
import Legend from '@/components/browse/Legend';
import { REFREPO_BRANCH } from '@/lib/refrepo';
import ResourcesTable from '@/components/browse/ResourcesTable';
import { UserData } from '@/lib/user';
import { defaultFetch } from '@/lib/request';
import { ggasAndSitesUrl } from '@/lib/routes';
import useSWR from 'swr';
import useUser from '@/hooks/useUser';

function groupToString(group: GroupType) {
  return group ? group.name : '';
}

function defaultGroupForUser(user: UserData): string | undefined {
  if (user.default_gga !== undefined) {
    return user.default_gga;
  } else if (user.gga.length > 0) {
    return user.gga[0];
  } else {
    return undefined;
  }
}

function BrowseRoot({ allGroups }: { allGroups: GGA[] }) {
  const { user, isLoading } = useUser();

  const groupFromString: StringToT<GroupType> = useMemo(() => {
    return val => allGroups.find(({ name }) => name === val) || null;
  }, [allGroups]);

  const [group, setGroup] =
    useParamValue<GroupType>('group', null, groupFromString, groupToString);

  useEffect(() => {
    if (!group && user) {
      const defaultGGA = defaultGroupForUser(user);
      if (defaultGGA === undefined) {
        return;
      }
      const foundGGA = allGroups.find((currentGGa: GGA) => currentGGa.name === user.default_gga);
      if (foundGGA) {
        setGroup(foundGGA);
      }
    }
  }, [user, group, setGroup, allGroups]);

  const allGroupsWithUserFirst: GGA[] = useMemo(() => {
    if (!user) {
      return allGroups;
    }
    const userGroups = user.gga.map(groupFromString).filter(g => g !== null) as GGA[];
    return userGroups.concat(allGroups.filter(({ name }) => user.gga.indexOf(name) === -1));
  }, [user, allGroups, groupFromString]);

  return (
    <Box sx={{ display: 'flex', flexDirection: 'column', m: 3 }}>
      {!group && !isLoading && (
        <Alert severity="info" sx={{ mb: 1 }}>
          We are unable to find out your group at this time, you are likely
          not connected. Please select your group in the selector below.
        </Alert>
      )}
      <Box sx={{mb: 2, display: 'flex' }}>
        <Legend />
        <Box sx={{flexGrow: 1 }}>
          <SelectGroup
            group={group}
            setGroup={setGroup}
            allGroups={allGroupsWithUserFirst}
            loading={!group && isLoading}
          />
        </Box>
      </Box>
      {group && (
        <ResourcesTable group={group} refrepoBranch={REFREPO_BRANCH} />
      )}
    </Box>
  );
}

function LoadingDataWrapper() {
  const { data } = useSWR(ggasAndSitesUrl(), defaultFetch, { suspense: true });
  // FIXME: handle error if data && !isLoading
  return (
    <BrowseRoot allGroups={data['ggas']} />
  );
}

export default function Browse() {
  // FIXME: have one static part that can be rendered with a basic explanation
  // of the page, and one container wrapped in a Suspense with the table.
  // The 'Suspense' is necessary because we use URLParams.
  return (
    <main>
      <Container maxWidth={false}>
        <Suspense fallback={<div>Loading...</div>}>
          <LoadingDataWrapper />
        </Suspense>
      </Container>
    </main>
  );
}

import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import InfoIcon from '@mui/icons-material/Info';
import Modal from "@mui/material/Modal";
import Typography from "@mui/material/Typography";
import { useState } from "react";

const modalStyle = {
  position: 'absolute' as 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: '80%',
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

export default function LegendButton() {
  const [helpOpen, setHelpOpen] = useState(false);
  return (
    <>
      <Button
        startIcon={<InfoIcon />}
        variant="text"
        size="small"
        sx={{alignSelf: 'center', mr: 2}}
        onClick={() => setHelpOpen(true)}
      >
        Legend
      </Button>
      <Modal
        open={helpOpen}
        onClose={() => setHelpOpen(false)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={modalStyle}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
                  Finding resources
          </Typography>
          <Typography id="modal-modal-description" sx={{ mt: 2 }} component="div" >
                  This page helps you browse the resources available on the platform,
                  including their site and the hardware available.
                  It also shows you what access you have to the various resources,
                  based on the Group you belong to.
                  Job submissions in the production queue are prioritized based
                  on who funded the material. There are four levels of priority,
                  each with a maximum job duration:
            <ul>
              <li>Prio 1: 168h (one week).</li>
              <li>Prio 2: 96h (three days).</li>
              <li>Prio 3: 48h (two days).</li>
              <li>Prio 4: 24h (one day).</li>
              <li>You may also submit a{' '}
                <a
                  href="https://www.grid5000.fr/w/Production#Can_I_use_besteffort_jobs_in_production_?"
                  target="_blank"
                  rel="noreferrer"
                >
                        best effort
                </a>
                {' '}job.
              </li>
            </ul>
                  The following access level also exists:
            <ul>
              <li>
                      No access: your Group doesn't grant you access to that resource.
              </li>
              <li>
                      Default: this resource is in the "default" queue, and does
                      not follow production access rules. You can have more
                      information about these rules {' '}
                <a
                  href="https://www.grid5000.fr/w/Grid5000:UsagePolicy#Rules_for_the_default_queue"
                  target="_blank"
                  rel="noreferrer"
                >
                        here
                </a>
                      .
              </li>
            </ul>
          </Typography>
        </Box>
      </Modal>
    </>
  );
}

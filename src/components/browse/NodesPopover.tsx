import IconButton from '@mui/material/IconButton';
import InfoIcon from '@mui/icons-material/Info';
import Popover from "@mui/material/Popover";
import Typography from "@mui/material/Typography";
import { useState } from "react";


export default function NodesPopover({ nodes }: { nodes: string[] }) {
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);

  const open = Boolean(anchorEl);
  const id = open ? 'nodes-popover' : undefined;
  // FIXME: remove this whole popover when abacus22 is indeed properly split
  const hasAbacus = nodes.some(n => n.startsWith('abacus22'));

  return (
    <div>
      {nodes.length}
      {hasAbacus && (
        <>
          <IconButton
            size="small"
            onClick={event => setAnchorEl(event.currentTarget)}
          >
            <InfoIcon fontSize="inherit" />
          </IconButton>
          <Popover
            id={id}
            open={open}
            anchorEl={anchorEl}
            anchorOrigin={{
              vertical: 'center',
              horizontal: 'right',
            }}
            transformOrigin={{
              vertical: 'center',
              horizontal: 'left',
            }}
            onClose={() => setAnchorEl(null)}
            disableRestoreFocus
          >
            <Typography sx={{ p: 1 }}>{nodes.join(',')}</Typography>
          </Popover>
        </>
      )}
    </div>
  );
}

import { Dispatch, HTMLAttributes } from "react";

import Autocomplete from '@mui/material/Autocomplete';
import CheckBoxIcon from '@mui/icons-material/CheckBox';
import CheckBoxOutlineBlankIcon from '@mui/icons-material/CheckBoxOutlineBlank';
import Checkbox from '@mui/material/Checkbox';
import Chip from '@mui/material/Chip';
import TextField from '@mui/material/TextField';

type ItemsState = Array<string>;

const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
const checkedIcon = <CheckBoxIcon fontSize="small" />;

export default function ItemSelector({ items, setItems, allItems, labelItems } : {
  items: ItemsState,
  setItems: Dispatch<ItemsState>,
  allItems: ItemsState,
  labelItems: string,
}) {
  return (
    <Autocomplete
      multiple
      id="checkboxes-tags-demo"
      options={allItems}
      value={items}
      onChange={(event, newValue) => setItems(newValue)}
      sx={{ minWidth: 300 }}
      size="small"
      disableCloseOnSelect
      renderOption={(props, option, { selected }) => {
        // 'key' is definitely in there, so cast with that attribute.
        const { key, ...others } = props as HTMLAttributes<HTMLLIElement> & {key: string};
        return (
          <li key={key} {...others}>
            <Checkbox
              icon={icon}
              checkedIcon={checkedIcon}
              style={{ marginRight: 8 }}
              checked={selected}
            />
            {option}
          </li>
        )}}
      renderTags={(tagValue, getTagProps) => tagValue.map((option, index) => (
        <Chip {...getTagProps({ index })} key={option} label={option} size="small" />
      ))}
      renderInput={(params) => (
        <TextField
          {...params}
          label={labelItems}
          variant="standard"
        />
      )}
    />
  );
}

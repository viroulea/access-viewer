import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardHeader from "@mui/material/CardHeader";
import { Job } from "@grid5000/refrepo-ts";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import { monikaShowJobUrl } from "@/lib/routes";
import { nodeNameFromAddress } from "@/lib/refrepo";

// This is meant to be reusable accross both the "browse" page and the upcoming
// platform usage page.
export default function JobCard({ job, site } : { job: Job, site: string }) {
  const {
    assigned_nodes,
    uid, user, name, project,
    queue, started_at, walltime,
    command, properties, mode, types,
  } = job;
  const startDate = new Date(started_at * 1000);
  const endDate = new Date((started_at + walltime) * 1000);
  return (
    <Card>
      <CardHeader
        title={
          <Typography gutterBottom variant="h5" component="div">
            Job {uid} ({user}, {project})
          </Typography>
        }
        subheader={
          <span>
            {startDate.toLocaleString()} -&gt; {endDate.toLocaleString()}
          </span>
        }
      />
      <CardContent>
        <Stack direction="column">
          {assigned_nodes && (
            <Box>
              <b>nodes</b>: {assigned_nodes.map(nodeNameFromAddress).join(', ')}
            </Box>
          )}
          {name && (
            <Box>
              <b>name</b>: {name}
            </Box>
          )}
          <Box>
            <b>queue</b>: {queue}
          </Box>
          <Box>
            <b>mode</b>: {mode}
          </Box>
          <Box sx={{ wordWrap: 'break-word' }}>
            <b>properties</b>: <span>{properties}</span>
          </Box>
          <Box>
            <b>types</b>: {types.join(', ')}
          </Box>
        </Stack>
      </CardContent>
      <CardActions>
        <Button
          size="small"
          rel="noreferrer"
          target="_blank"
          href={monikaShowJobUrl(site, uid)}
        >
          Show on Monika
        </Button>
      </CardActions>
    </Card>
  );
}

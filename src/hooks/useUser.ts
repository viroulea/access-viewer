import type { UserData } from '@/lib/user';
import { fetchWithCredentials } from '@/lib/request';
import { meUrl } from '@/lib/routes';
import useSWR from 'swr';

export default function useUser(): {
  user: UserData | undefined,
  isLoading: boolean,
  } {
  // We always try to authenticate the user, they can just escape the http
  // authentication if they don't wish/can't authenticate.
  const { data, error, isLoading } = useSWR(meUrl(), fetchWithCredentials);
  return {
    user: data ? data as UserData : undefined,
    isLoading,
  };
}

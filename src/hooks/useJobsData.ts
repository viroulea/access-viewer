import { Job, JobsAPIResponse } from "@grid5000/refrepo-ts";
import { RequestError, fetchUrlsOrErrorWithCredentials } from "@/lib/request";
import { clusterNameFromNodeAddress } from "@/lib/refrepo";
import { isFirst } from "@/lib/hardware";
import { jobsUrl } from "@/lib/routes";
import { useMemo } from "react";
import useSWR from "swr";

function isValidResponse(response: JobsAPIResponse | RequestError) {
  return 'items' in response;
}

function computeErrorPerSite(sites: string[], responses: (JobsAPIResponse | RequestError)[]): ErrorPerSite {

  return sites.reduce((res, s, idx) => {
    const siteResponse = responses[idx];
    if ('message' in siteResponse) {
      res[s] = siteResponse as RequestError;
    }
    return res;
  }, {} as ErrorPerSite);
}

function computeJobsPerCluster(responses: JobsAPIResponse[]): JobsPerCluster {
  return responses.reduce((result, response) => {
    const jobs = response as JobsAPIResponse;
    jobs.items.forEach(job => {
      const nodes = job.assigned_nodes;
      const clusters = [];
      if (nodes) {
        const clusters = nodes.map(clusterNameFromNodeAddress).filter(isFirst);
        clusters.forEach(c => {
          result[c] ||= [];
          result[c].push(job);
        });
      }
    });
    return result;
  }, {} as JobsPerCluster);
}

function jobsUrlsFromSites(sites: string[]) {
  if (sites.length === 0) {
    return undefined;
  }
  return sites.map(jobsUrl);
}

export type JobsPerCluster = {
  [name: string]: Job[],
};

export type ErrorPerSite = {
  [name: string]: RequestError,
};

export type JobsData = {
  perCluster: JobsPerCluster,
  errors: ErrorPerSite,
  loading: boolean,
};

export default function useJobsData(sitesUids: string[]): JobsData {
  const allJobsUrls = useMemo(() => jobsUrlsFromSites(sitesUids), [sitesUids]);
  const { data: jobsData, isLoading: loading } = useSWR(
    allJobsUrls, fetchUrlsOrErrorWithCredentials
  );

  // Compute error per cluster
  const [perCluster, errors]: [JobsPerCluster, ErrorPerSite] = useMemo(() => {
    if (jobsData) {
      return [
        computeJobsPerCluster(jobsData.filter(isValidResponse)),
        computeErrorPerSite(sitesUids, jobsData),
      ];
    } else {
      return [{}, {}];
    }
  }, [jobsData, sitesUids]);

  return { perCluster, errors, loading: allJobsUrls === undefined ? true : loading };
}

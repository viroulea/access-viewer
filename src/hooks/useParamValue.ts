import { Dispatch, useEffect, useState } from 'react';
import { useSearchParams } from 'next/navigation';

function interpolateT<T>(val: T) {
  return `${val}`;
}

export type TToString<T> = (val: T) => string;
export type StringToT<T> = (val: string) => T;

export function useParamValue<T>(
  param: string,
  initialValue: T,
  toT: StringToT<T>,
  toString: TToString<T> = interpolateT<T>,
  debounceDelay: number = 500,
): [T, Dispatch<T>, T] {

  const initialParams = useSearchParams();

  const defaultValue = toT(initialParams.get(param) || toString(initialValue));

  const [value, setValue] = useState<T>(defaultValue);
  const [debouncedValue, setDebouncedValue] = useState<T>(defaultValue);

  useEffect(() => {
    const timeoutId = setTimeout(() => {
      setDebouncedValue(value);
      // NOTE: using directly window.location.search because sharing
      // useSearchParams between hooks is quite buggy.
      const params = new URLSearchParams(window.location.search);
      const stringValue = value ? toString(value) : '';
      if (stringValue === '') {
        params.delete(param);
      } else {
        params.set(param, stringValue);
      }
      window.history.replaceState(null, '', `?${params.toString()}`);
    }, debounceDelay);
    return () => clearTimeout(timeoutId);
  }, [value, debounceDelay, param, toString, setDebouncedValue]);
  return [value, setValue, debouncedValue];
}

export function useStringParamValue(param: string, initialValue: string) {
  return useParamValue(param, initialValue, (val) => val, (val) => val);
}

export function useArrayStringParamValue(param: string, initialValue: Array<string>) {
  return useParamValue(
    param,
    initialValue,
    (val) => val.split(',').filter(v => v.length > 0),
    (val) => val.join(','));
}

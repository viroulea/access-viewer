import type { MetadataRoute } from 'next'
import { PUBLIC_URL } from '@/lib/routes';

export default function robots(): MetadataRoute.Robots {
  return {
    rules: {
      userAgent: '*',
      allow: '/',
    },
    sitemap: `${PUBLIC_URL}/sitemap.xml`,
  }
}

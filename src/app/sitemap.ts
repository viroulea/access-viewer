import { SitesAPIResponse, sitesUrl } from '@grid5000/refrepo-ts';
import type { MetadataRoute } from 'next';
import { PUBLIC_URL } from '@/lib/routes';
import { defaultFetch } from '@/lib/request';

// This sitemap is mostly here because the site is "new", and we want to help
// crawler all we can to let them know where the information is.
// https://developers.google.com/search/docs/crawling-indexing/sitemaps/overview#do-i-need-a-sitemap
// When we'll have more links to the website we can probably get rid of it
// as the navigations should be enough for robots to figure out our site.
export default async function sitemap(): Promise<MetadataRoute.Sitemap> {
  const sites = await defaultFetch(sitesUrl()) as SitesAPIResponse;

  const hardwareForSites: MetadataRoute.Sitemap = (sites.items || []).map(({ uid }) => ({
    url: `${PUBLIC_URL}/hardware/${uid}`,
    lastModified: new Date(),
    changeFrequency: 'weekly'
  }));

  return [
    {
      url: PUBLIC_URL,
      lastModified: new Date(),
      changeFrequency: 'weekly',
    },
    {
      url: `${PUBLIC_URL}/browse`,
      lastModified: new Date(),
      changeFrequency: 'weekly',
    },
    {
      url: `${PUBLIC_URL}/hardware`,
      lastModified: new Date(),
      changeFrequency: 'weekly',
    },
    ...hardwareForSites,
  ]
}

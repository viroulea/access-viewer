import type { Metadata } from "next";
import { ReactNode } from 'react';
import ResourcesExplorerAppBar from '@/components/AppBar';

export const metadata: Metadata = {
  title: "Resources Explorer - Browse",
  description: `Browse all the resources in Abaca, according to the user's access level. Display resources availability and running jobs on these nodes.`,
};

export default async function SiteLayout({
  children,
}: Readonly<{
  children: ReactNode;
}>) {
  return (
    <>
      <ResourcesExplorerAppBar autoSignIn />
      {children}
    </>
  );
}

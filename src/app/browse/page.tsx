import Browse from '@/components/browse/Browse';
import { SWRProvider } from '../swr-provider';
import { computeDefaultDataBrowse } from '@/lib/refdata';

export default async function BrowsePage() {
  // FIXME: have one static part that can be rendered with a basic explanation
  // of the page, and one container wrapped in a Suspense with the table.
  // The 'Suspense' is necessary because we use URLParams.
  const fallback = await computeDefaultDataBrowse();
  return (
    <SWRProvider value={fallback}>
      <Browse />
    </SWRProvider>
  );
}

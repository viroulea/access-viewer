"use client";

import Alert from '@mui/material/Alert';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActionArea from '@mui/material/CardActionArea';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Grid from '@mui/material/Unstable_Grid2';
import Link from 'next/link'
import MUILink from '@mui/material/Link';
import { ReactNode } from 'react';
import ResourcesExplorerAppBar from '@/components/AppBar';
import Typography from '@mui/material/Typography';
import { ganttUrl } from '@/lib/routes';

function LinkCard({
  title, children, href, notice
}: Readonly<{
  title: string,
  href: string,
  children: ReactNode,
  notice?: ReactNode,
}>) {
  return (
    <Card>
      <CardActionArea component={Link} href={href}>
        <CardContent>
          <Typography
            gutterBottom
            variant="h5"
            component="div"
            align="center"
          >
            {title}
          </Typography>
          <Typography variant="body1" component="div">
            {children}
          </Typography>
        </CardContent>
      </CardActionArea>
      {notice && (
        <CardActions>
          <Alert severity="info" sx={{width: 1}}>
            {notice}
          </Alert>
        </CardActions>
      )}
    </Card>
  );
}

export default function Home() {
  const ganttForSites = [
    { site: 'Grenoble', queues: [] },
    { site: 'Lille', queues: [] },
    { site: 'Lyon', queues: [] },
    { site: 'Nancy', queues: ['production'] },
    { site: 'Nantes', queues: [] },
    { site: 'Rennes', queues: ['production'] },
    { site: 'Sophia', queues: ['production'] },
    { site: 'Strasbourg', queues: [] },
    { site: 'Toulouse', queues: [] },
  ];

  return (
    <>
      <ResourcesExplorerAppBar />
      <main>
        <Grid
          container
          spacing={2}
          sx={{
            m: {
              lg: 1,
            }
          }}
          columns={3}
        >
          <Grid xs={3} lg={1}>
            <LinkCard title="Browse" href="/browse">
              This page allows you to quickly search and filter the node(s) you need on the platform, including the access level you have for them based on the group(s) you belong to.
            </LinkCard>
          </Grid>
          <Grid xs={3} lg={1}>
            <LinkCard title="Hardware" href="/hardware">
              Get a complete look at the hardware available through the platform,
              globally or per site.
            </LinkCard>
          </Grid>
          <Grid xs={3} lg={1}>
            <LinkCard
              title="Platform usage"
              href="/"
              notice={
                <>
                  This page is not implemented yet, but meanwhile you can take a
                  look at the "gantt" chart for the site and queue you're looking
                  for. Here are a few useful gantt chart links:
                  <Typography
                    style={{fontWeight: "bold"}}
                    component="div"
                  >
                   SLICES-FR (Default queue):
                  </Typography>
                  <Box
                    sx={{
                      display: 'flex',
                      justifyContent: 'space-evenly',
                    }}
                  >
                    {ganttForSites.map(({ site, queues }) => (
                      <MUILink
                        href={ganttUrl(site, [])}
                        target="_blank"
                        rel="noreferrer"
                        key={site}
                      >
                        {site}
                      </MUILink>
                    ))}
                  </Box>
                  <Typography
                    style={{fontWeight: "bold"}}
                    component="div"
                  >
                   Abaca (Production queue):
                  </Typography>
                  <Box
                    sx={{
                      display: 'flex',
                      justifyContent: 'space-evenly',
                    }}
                  >
                    {ganttForSites.map(({ site, queues }) => (
                      <MUILink
                        href={ganttUrl(site, queues)}
                        visibility= {queues.length === 0 ? 'hidden' : 'visible'}
                        target="_blank"
                        rel="noreferrer"
                        key={site}
                      >
                        {site}
                      </MUILink>
                    ))}
                  </Box>
                </>
              }
            >
              Get some live data about the platform usage, and figure out which
              interesting nodes are available now or at a future date!
            </LinkCard>
          </Grid>
        </Grid>
      </main>
    </>
  );
}

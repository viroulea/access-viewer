'use client';
import { ReactNode } from 'react';
import { RefType } from '@/lib/refdata';
import { SWRConfig } from 'swr'

export const SWRProvider = ({ children, value }: Readonly<{
  value: RefType;
  children: ReactNode;
}>) => {
  return (
    <SWRConfig value={value}>
      {children}
    </SWRConfig>
  );
};

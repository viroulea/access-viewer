import { ReactNode } from 'react';

import ResourcesExplorerAppBar from '@/components/AppBar';

export default async function SiteLayout({
  children,
}: Readonly<{
  children: ReactNode;
}>) {
  return (
    <>
      <ResourcesExplorerAppBar />
      {children}
    </>
  );
}

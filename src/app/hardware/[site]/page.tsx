import { SitesAPIResponse, sitesUrl } from '@grid5000/refrepo-ts';
import type { Metadata } from 'next';
import ShowSite from '@/components/hardware/site/ShowSite';
import { buildFullRefrepo } from '@/lib/refrepo';
import { defaultFetch } from '@/lib/request';

export type ParamsPageProps = {
  site: string;
};

export async function generateMetadata(
  { params }: { params: Promise<ParamsPageProps> }
): Promise<Metadata> {
  const site = (await params).site;
  return {
    title: `Abaca - Hardware description for ${site}`,
    description: `Full hardware description for ${site}, with the list of CPUs, GPUs, accelerators, architectures, network, storage, and so on.`,
  }
}

export default async function Site({ params } : { params: ParamsPageProps }) {
  const refrepo = await buildFullRefrepo([params.site]);
  return <ShowSite site={params.site} refrepo={refrepo} />;
}

export async function generateStaticParams() {
  const sites = await defaultFetch(sitesUrl()) as SitesAPIResponse;
  return sites.items.map(site => {
    return { site: site.uid };
  });
}

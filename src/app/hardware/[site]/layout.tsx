import { ReactNode, Suspense } from 'react';

import Menu from '@/components/hardware/common/Menu';
import type { ParamsPageProps } from './page';
import { getSites } from '@/lib/refrepo';

export default async function SiteLayout({
  children,
  params,
}: Readonly<{
  children: ReactNode;
  params: ParamsPageProps;
}>) {
  const sites = await getSites();
  return (
    <>
      <Suspense fallback={<div>Loading...</div>}>
        <Menu active={params.site} sites={sites} />
      </Suspense>
      {children}
    </>
  );
}

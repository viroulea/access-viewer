import GlobalHardware from '@/components/hardware/global/GlobalHardware';
import type { Metadata } from 'next';
import { buildFullRefrepo } from '@/lib/refrepo';

export const metadata: Metadata = {
  title: "Abaca - Global hardware description",
  description: "Global hardware description with statistics about processors, accelerators, network, storage, and so on available per site",
};

export default async function Hardware() {
  const refrepo = await buildFullRefrepo([]);
  return (
    <main>
      <GlobalHardware refrepo={refrepo} />
    </main>
  );
}


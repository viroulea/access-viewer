import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';

import type { Metadata, Viewport } from "next";

import { AppRouterCacheProvider } from '@mui/material-nextjs/v14-appRouter';
import CssBaseline from '@mui/material/CssBaseline';
import { ReactNode } from 'react';

export const metadata: Metadata = {
  title: "Abaca - Resources Explorer",
  description: "Explore hardware and available resources from Abaca",
};

export const viewport: Viewport = {
  width: 'device-width',
  initialScale: 1,
}

export default async function RootLayout({
  children,
}: Readonly<{
  children: ReactNode;
}>) {
  // NOTE: passing "prepend: true" here is important. It allows us to override
  // Material UI styles in components.
  return (
    <html lang="en">
      <body>
        <AppRouterCacheProvider options={{ prepend: true }}>
          <CssBaseline />
          {children}
        </AppRouterCacheProvider>
      </body>
    </html>
  );
}

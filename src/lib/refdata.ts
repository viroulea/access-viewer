import { APIResponse, ClusterAPI, ClusterNode, ClustersAPIResponse, GPUDevice, ObjectMap, SiteClusterPair, SitesAPIResponse, clustersUrl, nodesUrl, sitesUrl, sortSiteClusterPairs, sortSites } from '@grid5000/refrepo-ts';
import { accessesUrl, ggasAndSitesUrl } from '@/lib/routes';

import { REFREPO_BRANCH } from '@/lib/refrepo';
import { defaultFetch } from './request';
import { unstable_serialize } from 'swr';

export type RefType = {
  revalidateOnFocus: boolean,
  fallback: {
    [id: string]: any,
  },
};

type BasicGpuInfo = {
  model: string,
};

// Attempt to lower the amount of fallback data.
function extractGpus(gpus?: ObjectMap<GPUDevice>): ObjectMap<BasicGpuInfo> | undefined {
  if (gpus === undefined) {
    return gpus;
  }

  return Object.entries(gpus).reduce((res, [name, gpu]) => {
    res[name] = {
      model: gpu.model,
    };
    return res;
  }, {} as ObjectMap<BasicGpuInfo>);
}

// Attempt to lower the amount of fallback data.
function extractNodeData(response: APIResponse<ClusterNode>) {
  return {
    items: response.items.map(node => {
      const { uid, architecture, gpu_devices, nodeset, processor, supported_job_types } = node;
      return {
        uid,
        gpu_devices: extractGpus(gpu_devices),
        nodeset,
        architecture: {
          nb_procs: architecture.nb_procs,
        },
        processor: {
          other_description: processor.other_description,
        },
        supported_job_types: {
          queues: supported_job_types.queues,
        }
      };
    }),
  };
}

function extractClusterData(response: ClustersAPIResponse) {
  return {
    items: response.items.map((cluster: ClusterAPI) => {
      const { uid, queues } = cluster;
      return {
        uid,
        queues,
      };
    }),
  };
}

export async function computeDefaultDataBrowse() {
  const ggas = await defaultFetch(ggasAndSitesUrl());
  const accesses = await defaultFetch(accessesUrl());
  const sites = await defaultFetch(sitesUrl(REFREPO_BRANCH)) as SitesAPIResponse;
  const ref: RefType = {
    revalidateOnFocus: false,
    fallback: {
      [ggasAndSitesUrl()]: ggas,
      [accessesUrl()]: accesses,
      [sitesUrl(REFREPO_BRANCH)]: sites,
    },
  };

  const siteUids = sortSites(sites.items).map(({ uid }) => uid);
  const clusterUrls = siteUids.map(uid => clustersUrl(uid, REFREPO_BRANCH));
  const clustersKey = unstable_serialize(clusterUrls);
  const clustersData = await Promise.all(clusterUrls.map(defaultFetch));
  ref.fallback[clustersKey] = clustersData.map(extractClusterData);
  const flattenedClusters: SiteClusterPair[] = sortSiteClusterPairs(siteUids.flatMap((sUid, idx) => {
    const clustersForSite = (ref.fallback[clustersKey][idx] as ClustersAPIResponse).items.map(({ uid }) => uid);
    return clustersForSite.map((cUid: string) => ({
      sUid,
      cUid,
    }));
  }));

  const allNodesUrls = flattenedClusters.map(({ sUid, cUid }) => nodesUrl(sUid, cUid, REFREPO_BRANCH));
  const nodesKey = unstable_serialize(allNodesUrls);
  const nodesData = await Promise.all(allNodesUrls.map(defaultFetch));
  ref.fallback[nodesKey] = nodesData.map(extractNodeData);

  return ref;
}

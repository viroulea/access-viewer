import { APIResponse, Cluster, ClusterAPI, ClusterNode, ClustersAPIResponse, RefRepo, Site, SitesAPIResponse, clustersUrl, nodesUrl, sitesUrl } from "@grid5000/refrepo-ts";
import { defaultFetch } from "@/lib/request";

export const REFREPO_BRANCH = process.env.NEXT_PUBLIC_REFREPO_BRANCH || 'master';

export function nodeNameFromAddress(node: string) {
  return node.split('.')[0];
}

export function clusterNameFromNodeAddress(node: string) {
  return nodeNameFromAddress(node).split('-')[0];
}

export function getSitesUids(refrepo: RefRepo): string[] {
  return Object.values(refrepo.sites).map(({ uid }) => uid);
}

export async function buildCluster(site: string, cluster: ClusterAPI) {
  const nodes =
    (await defaultFetch(nodesUrl(site, cluster.uid, REFREPO_BRANCH))) as APIResponse<ClusterNode>;
  const ret: Cluster = {
    ...cluster,
    nodes: {},
  };
  nodes.items.forEach(n => ret.nodes[n.uid] = n);
  return ret;
}

// The full refrepo is too big for nextjs's cache (>2MB), since we generate
// data at build time anyway, we might as well create a lot of cachable fetches.
export async function buildSite(s: string) {
  const clusters = (await defaultFetch(clustersUrl(s, REFREPO_BRANCH))) as ClustersAPIResponse;

  const site: Site = {
    uid: s,
    clusters: {},
  };
  for (const c of clusters.items) {
    site.clusters[c.uid] = await buildCluster(s, c);
  }
  return site;
}

export async function getSites() {
  const sites = (await defaultFetch(sitesUrl(REFREPO_BRANCH))) as SitesAPIResponse;
  return sites.items.map(({ uid }) => uid);
}

export async function buildFullRefrepo(onlySites: string[]) {
  const sites = onlySites.length > 0 ? onlySites : await getSites();
  const refrepo: RefRepo = {
    sites: {},
  };
  for (const s of sites) {
    refrepo.sites[s] = await buildSite(s);
  }
  return refrepo;
}

import { Cluster, ClusterNode, GPUDevice, Network, OtherDevice, Processor, RamSlot, RefRepo, Storage } from "@grid5000/refrepo-ts";

export type Summary = {
  nSites: number,
  nClusters: number,
  nNodes: number,
  nCores: number,
  nGPUs: number,
  nGPUsCores: number,
  ram: number,
  pmem: number,
  ssds: number,
  hdds: number,
  storage: number,
  flops: number,
};

export type Interval = {
  from: number,
  to: number,
};

export type SimilarNodesDescription = {
  ids: number[],
  node: ClusterNode,
};

export function nodeIdAsNumber(cluster: Cluster, nodeId: string): number {
  return parseInt(nodeId.replace(`${cluster.uid}-`, ''), 10);
}

export type GPUById = {
  [id: string]: { n: number, gpu: GPUDevice };
};

export function createEmptySummary(): Summary {
  return {
    nSites: 0,
    nClusters: 0,
    nNodes: 0,
    nCores: 0,
    nGPUs: 0,
    nGPUsCores: 0,
    ram: 0,
    pmem: 0,
    ssds: 0,
    hdds: 0,
    storage: 0,
    flops: 0,
  };
}

export function isDram(ram: RamSlot) {
  return ram.technology === 'dram';
}

export function isPmem(ram: RamSlot) {
  return ram.technology === 'pmem';
}

export function isHdd(disk: Storage) {
  return disk.storage === 'HDD';
}

export function isSsd(disk: Storage) {
  return disk.storage === 'SSD';
}

export function isPrimary(disk: Storage) {
  return disk.id === 'disk0';
}

export function sizeReducer(sum: number, elem: RamSlot | Storage) {
  return sum + elem.size;
}

type Breakpoint = [number, string];
type System = {
  step: number,
  breakpoints: Breakpoint[],
};

const METRICS_BREAKPOINTS: System = {
  step: 10**3,
  breakpoints: [[10**9, 'MB'], [10**12, 'GB'], [10**15, 'TB'], [10**18, 'PB']],
};

const IEC_BREAKPOINTS: System = {
  step: 2**10,
  breakpoints: [[2**30, 'MiB'], [2**40, 'GiB'], [2**50, 'TiB'], [2**60, 'PiB']],
};

function fixedSize(size: number, step: number, limit: number, fix: number) {
  return ((size * step) / limit).toFixed(fix);
}

export function formatDate(date: string) {
  const d = new Date(date);
  return d.toISOString().split('T')[0];
}

export function formatRate(rate: number) {
  const rateInMbps = rate / 10 ** 6;
  if (rateInMbps < 1000) {
    return `${rateInMbps} Mbps`;
  } else {
    return `${rateInMbps/1000} Gbps`;
  }
}

export function formatSize(size: number, unit: 'metric' | 'IEC' = 'IEC',
  fix: number = 2) {
  const system = unit === 'metric' ? METRICS_BREAKPOINTS : IEC_BREAKPOINTS;
  for (const breakpoint of system.breakpoints) {
    const [limit, suffix] = breakpoint;
    if (size < limit) {
      return `${fixedSize(size, system.step, limit, fix)} ${suffix}`;
    }
  }
  // We crossed all breakpoints, just use the last one known.
  const [limit, suffix] = system.breakpoints[system.breakpoints.length - 1];
  return `${fixedSize(size, system.step, limit, fix)} ${suffix}`;
}

export function isSupported(device: GPUDevice) {
  const { compute_capability } = device;
  return !compute_capability || compute_capability > 5.2;
}

export function formatGPUAsString(device: GPUDevice): string {
  const { vendor, model, memory } = device;
  return `${vendor} ${model} (${formatSize(memory, 'IEC', 0)})`;
}

export function formatOther(device: OtherDevice) {
  const { vendor, model } = device;
  return `${vendor} ${model}`;
}

export function formatFlops(flops: number) {
  return `${(flops / 10**12).toFixed(1)} TFLOPS`;
}

export function formatMem(ram: number, pmem: number,
  unit: 'metric' | 'IEC' = 'IEC', fix: number = 2) {
  const pmemString = pmem > 0 ? ` + ${formatSize(pmem, unit, fix)} PMEM` : '';
  return `${formatSize(ram, unit, fix)} RAM${pmemString}`;
}

export function computeSummary(refrepo: RefRepo,
  filterCluster = (cluster: Cluster) => true): Summary {
  const ret = createEmptySummary();
  Object.values(refrepo.sites).forEach(site => {
    const clusters = Object.values(site.clusters).filter(filterCluster);
    if (clusters.length > 0) {
      ret.nSites += 1;
    }
    clusters.forEach(cluster => {
      const nodes = Object.values(cluster.nodes);
      if (nodes.length > 0) {
        ret.nClusters += 1;
      }
      nodes.forEach(node => {
        ret.nNodes += 1;
        ret.nCores += node.architecture.nb_cores;
        ret.ram += node.memory_devices.filter(isDram).reduce(sizeReducer, 0);
        ret.pmem += node.memory_devices.filter(isPmem).reduce(sizeReducer, 0);
        ret.storage += node.storage_devices.reduce(sizeReducer, 0);
        ret.hdds += node.storage_devices.filter(isHdd).length;
        ret.ssds += node.storage_devices.filter(isSsd).length;
        ret.flops += node.performance.node_flops;
        if (node.gpu_devices) {
          const GPUs = Object.values(node.gpu_devices);
          ret.nGPUs += GPUs.length;
          ret.nGPUsCores += GPUs.reduce((sum, { cores }) => sum + cores, 0);
        }
      });
    });
  });
  return ret;
}

export function inProduction(cluster: Cluster) {
  return cluster.queues.includes('production');
}

export function inDefault(cluster: Cluster) {
  return cluster.queues.includes('default');
}

export function inTesting(cluster: Cluster) {
  return cluster.queues.includes('testing');
}

export function isFirst<T>(value: T, index: number, array: T[]) {
  return array.indexOf(value) === index;
}

export function computeReservation(site: string, cluster: Cluster) {
  const { uid, queues, exotic } = cluster;
  const args: string[] = ['oarsub', '-p', uid];
  if (exotic) {
    args.push('-t exotic');
  }
  if (queues.includes('testing')) {
    args.push('-q testing');
  } else if (queues.includes('production')) {
    args.push('-q production');
  }
  args.push('-I');
  return args.join(' ');
}

export function computeIntervals(unsortedIds: number[]): Interval[] {
  // Because js turns elements to string by default........
  const compareNumber = (a: number, b: number) => a - b;
  const ids = unsortedIds.sort(compareNumber);

  let lastNodeId = ids[0];
  let intervalBegin = lastNodeId;
  const intervals: Array<{ from: number, to: number }> = [];
  for (const id of ids.slice(1)) {
    if (id !== lastNodeId + 1) {
      // We need to create a new interval
      intervals.push({ from: intervalBegin, to: lastNodeId });
      intervalBegin = id;
    }
    lastNodeId = id;
  }
  // Push the last interval
  intervals.push({ from: intervalBegin, to: lastNodeId });
  return intervals;
}

export function formatIntervals(intervals: Interval[]): string {
  const joinedIntervals = intervals.map(
    ({ from, to }) => (from === to) ? `${from}` : `${from}-${to}`
  ).join(',');
  const brackets = intervals.some(({ from, to }) => from !== to);
  return brackets ? `[${joinedIntervals}]` : joinedIntervals;
}

export function visibleNetwork({ enabled, management, mounted, mountable }: Network) {
  return enabled && !management && (mounted || mountable);
}

// This function creates a structure identifying a GPU (ie: two GPUs with the
// same identifier are considered similar, even if some small details like
// the bios version of the device name might be different.
export function createGPUIdentifier(gpu: GPUDevice) {
  const { vendor, model, compute_capability, memory, cores } = gpu;
  return {
    vendor, model, compute_capability, memory, cores
  };
}

// Just like for the GPU, creates a structure able to identify similar
// storages.
export function createStorageIdentifier(dev: Storage) {
  const { id, storage, reservation, model, vendor, size } = dev;
  return {
    interface: dev.interface,
    id, storage, reservation, model, vendor, size
  };
}

// Just like for the GPU, creates a structure able to identify similar
// network interfaces.
export function createNetworkIdentifier(net: Network) {
  const {
    device, driver, kavlan, enabled, management, mountable, mounted, rate,
    sriov, name, vendor
  } = net;
  return {
    interface: net.interface,
    device, driver, kavlan, enabled, management, mountable, mounted, rate,
    sriov, name, vendor,
  };
}

// Helper function to extract only the relevant properties from a Processor.
// The API includes more information than what is described in the type, so we
// need to extract only what we want to take into account to differenciate them.
export function createProcessorIdentifier(proc: Processor) {
  const { other_description, model, microarchitecture, version, clock_speed } = proc;
  return {
    other_description, model, microarchitecture, version, clock_speed,
  };
}

// Create a structure identifying a node (ie: two nodes with the same structure
// are similar, even if some other details might be different).
export function createNodeIdentifier(node: ClusterNode) {
  // This function extracts relevant node attributes to identify if nodes
  // should be considered similar or not.
  // It returns a string representation of the node that can be used to compare
  // them.
  const {
    network_adapters, storage_devices,
    architecture, nodeset, gpu_devices, other_devices,
    memory_devices, performance, processor,
  } = node;

  return {
    proc: createProcessorIdentifier(processor),
    architecture, nodeset, other_devices,
    memory_devices, performance,
    gpus: gpu_devices ? Object.values(gpu_devices).map(createGPUIdentifier) : undefined,
    network: network_adapters.map(createNetworkIdentifier),
    storage: storage_devices.map(createStorageIdentifier),
  };
}

export function groupGPUsById(node: ClusterNode): GPUById {
  const gpuById: GPUById = {};
  if (node.gpu_devices) {
    Object.values(node.gpu_devices).forEach(gpu => {
      const gpuId = JSON.stringify(createGPUIdentifier(gpu));
      gpuById[gpuId] ||= { n: 0, gpu };
      gpuById[gpuId].n += 1;
    });
  }
  return gpuById;
}

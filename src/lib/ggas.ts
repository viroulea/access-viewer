export type GGA = {
  access_level: string,
  name: string,
  site: string,
};

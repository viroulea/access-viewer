// The json has an object which has nodeset as key, linking to a gga->number
type AccessDescription = {
  [accesses: string]: {
    level: number,
    label: string,
  },
};

export type Accesses = {
  [gga: string]: AccessDescription,
};

export function accessValueToString(fullLabel: string | undefined): string {
  if (!fullLabel) {
    return 'undefined';
  }
  const [mode, level] = fullLabel.split('-');
  switch (mode) {
  case 'besteffort':
    return 'Best effort';
  case 'noaccess':
    return 'No access';
  case 'p1':
  case 'p2':
  case 'p3':
  case 'p4':
    return `P${mode.substring(1)}`;
  case 'default':
    return `Default (${level})`;
  default:
    return `${fullLabel}`;
  }
}

// This function is used for sorting, a lower number indicates a value that
// should come first.
const SORTED_ACCESSES = [
  'p1', 'p2', 'p3', 'p4',
  'default-bronze', 'default-silver', 'default-gold',
  'besteffort',
];
export function accessValueToNumber(value: string): number {
  const index = SORTED_ACCESSES.indexOf(value);
  return index < 0 ? SORTED_ACCESSES.length : index;
}

export function accessLabelToValue(label: string | undefined, level: string): string {
  switch (label) {
  case 'besteffort':
  case 'no-access':
    return label.replace('-', '');
  case 'default':
    return `default-${level}`;
  case 'p1':
  case 'p2':
  case 'p3':
  case 'p4':
    return label;
  default:
    return `unknown-${label}`;
  }
}

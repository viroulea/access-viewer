import { ReactNode } from "react";

export type BasicHeader = {
  field: string,
  element: ReactNode,
};

export type GroupedHeaders = {
  element: ReactNode,
  headers: BasicHeader[],
};

export type Header = BasicHeader | GroupedHeaders;

export function isGroupedHeader(h: Header): boolean {
  return (h as GroupedHeaders).headers !== undefined;
}

export type Row = {
  [id: string]: {
    value: number | string,
    element?: ReactNode,
  },
};

export function descendingComparator(a: Row, b: Row, orderBy: string) {
  const bVal = b[orderBy].value;
  const aVal = a[orderBy].value;
  if (bVal < aVal) {
    return -1;
  }
  if (bVal > aVal) {
    return 1;
  }
  return 0;
}

export type Order = 'asc' | 'desc';

export function getComparator(order: Order, orderBy: string) {
  return order === 'desc'
    ? (a: Row, b: Row) => descendingComparator(a, b, orderBy)
    : (a: Row, b: Row) => -descendingComparator(a, b, orderBy);
}

export function capitalize(s: string): string {
  return `${s.charAt(0).toUpperCase()}${s.slice(1)}`;
}

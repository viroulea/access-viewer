export type RequestError = {
  status: number,
  message: string,
};

export function fetchOrErrorWithCredentials(url: string, options: any = {}) {
  return fetch(url, {
    ...options,
    credentials: 'include',
    // This prevent an annoying behavior where the user is redirected often when
    // not authenticated. It's fine because if viewing on api.g5k the user *will*
    // be already authenticated, so this redirect will only occur when viewing on
    // a public url, where we're typically not authenticated.
    redirect: 'error',
  }).then(r => {
    if (!r.ok) {
      return {
        status: r.status,
        message: `Response not ok: ${r.statusText}`,
      };
    }
    return r.json();
  }).catch(e => {
    return {
      status: 0,
      message: e.message,
    }
  });
}

export function fetchUrlsOrErrorWithCredentials(urls: string[], options: any = {}) {
  return Promise.all(urls.map(fetchOrErrorWithCredentials));
}

export function defaultFetch(url: string, options: any = {}) {
  // FIXME: this effectively just ignores error; we should have a snackbar of
  // some sort.
  // This should never violently crash to the user though, because we use
  // fallback data and those will be used.
  return fetch(url, options).then(r => {
    if (!r.ok) {
      throw new Error("Response was not ok");
    }
    return r.json();
  }).catch(e => { console.error(e); throw e; });
}

export function fetchWithCredentials(url: string, options: any = {}) {
  return defaultFetch(url, {
    ...options,
    credentials: 'include',
    // This prevent an annoying behavior where the user is redirected often when
    // not authenticated. It's fine because if viewing on api.g5k the user *will*
    // be already authenticated, so this redirect will only occur when viewing on
    // a public url, where we're typically not authenticated.
    redirect: 'error',
  });
}

export function defaultFetchUrls(urls: string) {
  return Promise.all(urls.split(',').map(url => fetchWithCredentials(url)));
}

export type UserData = {
  first_name: string,
  last_name: string,
  gga: string[],
  default_gga?: string,
};

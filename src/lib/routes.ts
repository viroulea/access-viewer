import { apiUrl } from "@grid5000/refrepo-ts";

// The public url of that website, the one *not* behind http authentication.
export const PUBLIC_URL = 'https://public-api.grid5000.fr/explorer';

export function monikaShowJobUrl(site: string, jobId: number) {
  return `https://intranet.grid5000.fr/oar/${site}/monika.cgi?job=${jobId}`;
}

export function ganttUrl(site: string, queues: string[], clusterUid?: string) {
  const queueSuffix = queues.includes('production') ? '-prod' : '';
  const filterSuffix = clusterUid ? `?filter=${clusterUid} only` : '';
  return `https://intranet.grid5000.fr/oar/${site}/drawgantt-svg${queueSuffix}/${filterSuffix}`;
}

export function jobsUrl(site: string) {
  return apiUrl(`/sites/${site}/jobs?state=running&resources=yes`, undefined, 'https://api.grid5000.fr/stable');
}

export function umsUrl(path: string) {
  return `${process.env.NEXT_PUBLIC_UMS_ROOT}${path}`;
}

export function meUrl() {
  return umsUrl('/users/~');
}

export function ggasAndSitesUrl() {
  return apiUrl('/users/ggas_and_sites');
}

export function accessesUrl() {
  return apiUrl('/accesses');
}

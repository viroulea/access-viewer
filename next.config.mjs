/** @type {import('next').NextConfig} */
const nextConfig = {
  output: 'export',
  basePath: process.env.WEBSITE_PREFIX || '',
  // The website is deployed on an apache server, this is easier than figuring
  // out how to tune the configuration.
  // It makes nextjs emit '/page' in '/page/index.html' instead of '/page.html'.
  trailingSlash: true,
};

export default nextConfig;

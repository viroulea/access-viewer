Online: https://grid5000.gitlabpages.inria.fr/resources-explorer/


This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Development notes

Some useful `jq` queries until we connect properly to the API:
```bash
# To pluck useful fields from all ggas (https://api.grid5000.fr/stable/users/groups.json?is_gga=true)
jq '[.items[] | {name, site, access_level}]'
```

For the refrepo I couldn't make an easy jq query, so I filtered the refrepo in ruby:

```ruby
refrepo = load_data_hierarchy.slice('sites')
refrepo['sites'].each do |sname, sentry|
  sentry['clusters'].each do |cname, centry|
    centry['nodes'].each do |nname, node|
      centry['nodes'][nname] = node.slice('uid', 'gpu_devices', 'processor', 'architecture', 'nodeset')
    end
    sentry['clusters'][cname] = centry.slice('uid', 'nodes', 'queues')
  end
  refrepo['sites'][sname] = sentry.slice('uid', 'clusters')
end

File.open("#{options[:output]}/full.json", 'w') do |file|
  JSON.dump(refrepo, file)
end
```

### Testing the generated deb

Create the deb:
```bash
DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage -us -uc
```

Start a container with port redirection and the local folder mounted:
```bash
docker run -p 5423:5423 -v `pwd`:/titi -it debian:bookworm bash
```
Then apt-install python3, dpkg -i the generated deb, and:
```bash
python3 -m http.server 5423
```
(should work fine except the fact that the links all have the `/explorer` prefix, which is expected)

### Testing with locally running UMS

You can use a `.env.development.local` to override some, or all, of the environment variables.
For instance if you have UMS running at `localhost:3000` you can tell resources explorer to use it with the following `.env.development.local`:
```
NEXT_PUBLIC_UMS_ROOT=http://localhost:3000
```

### Authenticating locally when targeting prod UMS

If you're using a locally running UMS you don't need authentication as it's handled in vagrant, but if you're using UMS from prod you need to set the `Authorization` header when browsing resources explorer (it's forwarded automatically to UMS).

I used an extension to set the request header locally (which is expected to be set by our apache server in api-proxy).
I configured it so that url matching `http://localhost:3210/*` would have the `Authorization` header set to `Basic abcd` (where abcd is an actual token value!).

### Testing a custom branch of the reference repository

This is especially useful if testing out new accesses generation.
There is a variable `REFREPO_BRANCH` in the GitLab CI, that you can override (for instance) during the push:

```
git push -o ci.variable="REFREPO_BRANCH=mybranch"
```

You may also add a temporary commit changing the variable value in the `.gitlab-ci.yml`.

### Releasing a new version of Resources Explorer

Releasing Resources Explorer might be needed even if there were no code changes, for instance if a GGA was added/removed in UMS, or if some part of the reference repository changed.

Here are the few steps currently needed to release Resources Explorer:
  - Edit `debian/changelog` to bump the version and change the release date
  - Commit and push your changes to `master`
  At this point the pipeline will start, build the website, deploy it to gitlab pages, and build the debian package. The job to push the debian package will be created, but do not start it just yet.
  - Create a named `v<versionnumber>` (eg: `v0.1.11`)
  - Manually start the `push-bookworm-package` job
  - Go to platforms/production (`cdgp`+`cdpl`), and edit/commit the new version number in `hieradata/common.yaml`.
  - Update puppet, and run puppet on `api-north`, `api-south`, and all `api-proxy`-s.

This should make sure all mirrors are properly deployed.
